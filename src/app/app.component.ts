import { Component, ViewChild, Output, EventEmitter } from "@angular/core";
import {
  App,
  Nav,
  Platform,
  AlertController,
  Content,
  ModalController
} from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AuthProvider } from "../providers/auth/auth";
import { FcmProvider } from "../providers/fcm/fcm";
import "rxjs/add/operator/takeUntil";
import { errorHandler } from "@angular/platform-browser/src/browser";
import { Globalization } from "@ionic-native/globalization";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/v4";

declare var cordova: any;
declare var window: any;
// import { Firebase } from "@ionic-native/firebase";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content)
  content: Content;

  rootPage: any;

  pages: Array<{ title: string; component: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public alertCtrl: AlertController,
    public auth: AuthProvider,
    public fcmp: FcmProvider,
    // private firebase: Firebase,
    public modalCtrl: ModalController,
    public app: App,
    private globalization: Globalization,
    public fcm: FCM
  ) {
    this.startengine();
  }
  startengine() {
    this.auth.clearcookie();
    this.auth.removeads();
    if (this.auth.isLogin()) {
      this.generatefcm();
      this.rootPage = "PropertieslistviewPage";
      if (this.auth.isOwner() == false) {
        this.auth.removepropertynotification();
        this.auth.removeslotpropertynotification();
      }
    } else {
      this.rootPage = "LoginPage";
    }
    this.initializeApp();
    this.getmore();
  }

  gotit: boolean = false;
  logintoken: any;
  logintokenuser: any;
  logintokenSubscribe: any;
  logintokenuserSubscribe: any;
  generatefcm() {
    var tis = this
    this.platform.ready().then(() => {
    if (this.platform.is("cordova")) {
      this.logintoken = this.fcmp.checkusers().valueChanges();
      this.logintokenSubscribe = this.logintoken.subscribe(queriedItems => {
        debugger;
        if (queriedItems.length == 0) {
          tis.fcmp.getToken();
        } else if (queriedItems.length > 1) {
          tis.usersubscruser = this.fcmp.checkusers().snapshotChanges();
          tis.usersubscruserSubscribe = this.usersubscruser.subscribe(
            queriedItemss => {
              debugger;
              queriedItemss.forEach(action => {
                tis.pleaseunsubscribe();
                tis.fcmp.deleteToken(action.key);
                tis.auth.logout();
                tis.app
                  .getRootNav()
                  .setRoot("LoginPage", { dayleasing_logout: "logout" });
              });
            }
          );
        } else {
          this.logintokenuser = this.fcmp.checkusers().snapshotChanges();
          this.logintokenuserSubscribe = this.logintokenuser.subscribe(
            queriedItems => {
              queriedItems.forEach(action => {
                this.fcmp.updateToken(action.key);
              });
            }
          );
        }
        if (this.auth.isLogin() == false) {
          this.logintokenSubscribe.unsubscribe();
          this.logintokenuserSubscribe.unsubscribe();
        } else {
        }
      });
    }
  })
  }

  isadmin() {
    if (
      this.auth.getProfile() &&
      JSON.parse(this.auth.getProfile()).wp_capabilities[0]
    ) {
      if (
        JSON.parse(this.auth.getProfile()).wp_capabilities[0].includes(
          "administrator"
        )
      ) {
        return true;
      } else {
        return false;
      }
    }
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.fcmp.generatefcmtoken();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (this.platform.is("cordova")) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        var ee: any = this;
        cordova.getAppVersion.getVersionNumber(function(version) {
          if (localStorage.getItem("dayleasingAppversion")) {
            if (
              JSON.parse(localStorage.getItem("dayleasingAppversion")) ==
              version
            ) {
            } else {
              ee.auth.logout();
              ee.rootPage = "LoginPage";
            }
          }
          setTimeout(() => {
            localStorage.setItem(
              "dayleasingAppversion",
              JSON.stringify(version)
            );
          }, 1000);
        });
        // Firebase
        var tisss = this;     
        this.fcm.onNotification().subscribe((notification) => {
          console.log(JSON.stringify(notification));   
          if (notification.tap == false) {
            tisss.reachedproperty("", notification.body);
          } else {
            debugger;
            if (notification && notification.body) {
              debugger;
              tisss.reachedproperty("", notification.body);
            }
          }
        });
        this.fcm.requestPushPermission();
        // window.FirebasePlugin.onNotificationOpen(
        //   function(notification) {
        //     console.log(JSON.stringify(notification));
        //     if (notification.tap == false) {
        //       tisss.reachedproperty("", notification.body);
        //     } else {
        //       debugger;
        //       if (notification && notification.body) {
        //         debugger;
        //         tisss.reachedproperty("", notification.body);
        //       }
        //     }
        //   },
        //   function(error) {
        //     console.error(error);
        //   }
        // );

        var options = { formatLength: "short", selector: "date and time" };
        this.globalization
          .getDatePattern(options)
          .then(res => {
            localStorage.setItem(
              "iana_timezone",
              JSON.stringify(res.iana_timezone)
            );
          })
          .catch(e => console.log(e));
      }
    });
  }

  /////////////////////////////////////////
  /////////////Got Notification////////////
  ////////////////////////////////////////
  backactive: boolean = false;
  reachedproperty(type, message) {
    this.backactive = true;
    var slotModal: any = this.modalCtrl.create(
      "SendpushPage",
      {
        type: 1,
        message: message
      },
      { cssClass: "reachmodal" }
    );
    slotModal.present();
    slotModal.onDidDismiss(data => {
      this.backactive = false;
    });
  }
  gotohome(page) {
    this.nav.setRoot(page);
  }
  goto(page) {
    this.nav.push(page);
  }
  usersubscr: any;
  usersubscruser: any;
  usersubscrSubscribe: any;
  usersubscruserSubscribe: any;
  gotologout(page) {
    // this.auth.logout();
    // this.nav.push(page, { dayleasing_logout: "logout" });
    this.usersubscr = this.fcmp.checkusers().valueChanges();
    this.usersubscrSubscribe = this.usersubscr.subscribe(queriedItems => {
      if (this.auth.isLogin() && queriedItems) {
        debugger;
        if (queriedItems.length == 0) {
          this.pleaseunsubscribe();
          this.auth.logout();
          this.app.getRootNav().setRoot(page, { dayleasing_logout: "logout" });
        } else if (queriedItems.length > 1) {
          this.pleaseunsubscribe();
          this.usersubscruser = this.fcmp.checkusers().snapshotChanges();
          this.usersubscruserSubscribe = this.usersubscruser.subscribe(
            queriedItems => {
              queriedItems.forEach(action => {
                this.fcmp.deleteToken(action.key);
                this.pleaseunsubscribe();
              });
            }
          );
          this.auth.logout();
          this.app.getRootNav().setRoot(page, { dayleasing_logout: "logout" });
        } else {
          this.usersubscruser = this.fcmp.checkusers().snapshotChanges();
          this.usersubscruserSubscribe = this.usersubscruser.subscribe(
            queriedItems => {
              queriedItems.forEach(action => {
                this.fcmp.deleteToken(action.key);
                this.pleaseunsubscribe();
              });
            }
          );
          this.auth.logout();
          this.nav.push(page, { dayleasing_logout: "logout" });
        }
      } else if (this.auth.isLogin() && !queriedItems) {
        this.auth.logout();
        this.nav.push(page, { dayleasing_logout: "logout" });
      }
    });
  }
  logout(page) {
    let alert = this.alertCtrl.create({
      title: "Confirm Logout",
      message: "Do you want to logout?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          handler: () => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.gotologout(page);
          }
        }
      ]
    });
    alert.present();
  }

  async closeApp(message) {
    var alert = await this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: "Continue",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Close",
          handler: () => {
            this.pleaseclose();
          }
        }
      ]
    });

    await alert.present();
  }
  pleaseunsubscribe() {
    // if (this.usersubscrSubscribe) {
    //   this.usersubscrSubscribe.unsubscribe();
    // }
    // if (this.usersubscruserSubscribe) {
    //   this.usersubscruserSubscribe.unsubscribe();
    // }
  }
  pleaseclose() {
    this.platform.exitApp();
  }

  ////////////////////////
  /////Search Keyword/////
  ///////////////////////
  searchkey: string = "";
  searchkeyword() {
    if (this.searchkey) {
      this.auth.startloader();
      try {
        this.auth.searchkeyword(this.searchkey).subscribe(
          data => {
            this.auth.stoploader();
            if (data.json() && data.json().status == 1) {
              this.nav.push("KeywordsrchPage", {
                searchdata: data.json().data,
                resultfor: this.searchkey
              });
              this.searchkey = "";
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
      } catch (err) {
        this.auth.stoploader();
      }
    }
  }

  ////////////////////////
  ///////More pages///////
  ///////////////////////
  moremenus: any = [];
  getmore() {
    this.auth.startloader();
    try {
      this.auth.getmore(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data.json() && data.json().status == 1) {
            data.json().menu.forEach((item, index) => {
              if (item.menu_item_parent == "2535") {
                this.moremenus.push(item);
              }
            });
          }
        },
        errorHandler => {
          this.auth.stoploader();
        }
      );
    } catch (err) {
      this.auth.stoploader();
    }
  }
  showmore: boolean = false;
  getmoreacc() {
    this.showmore = !this.showmore;
    // setTimeout(() => {
    //   this.content.scrollToBottom();
    // }, 100);
  }

  //////////////////////////////
  ///////More detail page///////
  //////////////////////////////
  taketomore(menu_item_id, object_id, object) {
    this.auth.startloader();
    try {
      this.auth
        .getmoredetail(this.auth.getuserId(), menu_item_id, object_id, object)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (
              data.json() &&
              data.json().status == 1 &&
              data.json().menu_item_data &&
              data.json().menu_item_data.length
            ) {
              this.nav.push("MorePage", {
                searchdata: data.json().menu_item_data[0]
              });
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
    }
  }
}
