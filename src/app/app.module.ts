import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { MyApp } from "./app.component";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
// Providers
import { ApiProvider } from "../providers/api/api";
import { ApicallsProvider } from "../providers/apicalls/apicalls";
import { TracklocationProvider } from "../providers/tracklocation/tracklocation";
// Plugins
import { NativePageTransitions } from "@ionic-native/native-page-transitions";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../providers/auth/auth";
import { AgmCoreModules } from "../agm/core";
import { AgmCoreModule } from "@agm/core";
import { AgmDirectionModule } from "agm-direction";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { SocialSharing } from "@ionic-native/social-sharing";
import { GoogleMapsAPIWrapper } from "@agm/core";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { File } from "@ionic-native/file";
import { Camera } from "@ionic-native/camera";
import { DatePipe } from "@angular/common";
import { Vibration } from "@ionic-native/vibration";
import { NativeGeocoder } from "@ionic-native/native-geocoder";
import { AutoCompleteModule } from "ionic2-auto-complete";
import { CompleteTestServiceProvider } from "../providers/complete-test-service/complete-test-service";
import { GoogleMapsProvider } from "../providers/google-maps/google-maps";
import { OrderModule } from "ngx-order-pipe";
import { PayPal } from "@ionic-native/paypal";
import * as firebase from "firebase/app";
import "firebase/database"; // If using Firebase database
import "firebase/storage"; // If using Firebase storage
import "firebase/auth";
import "firebase/firestore";
import "firebase/messaging";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { FcmProvider } from "../providers/fcm/fcm";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { FirestoreSettingsToken } from "@angular/fire/firestore";
import { CountdownModule } from "ngx-countdown";
import { PinchZoomModule } from "ngx-pinch-zoom";
import { NetworkInterface } from "@ionic-native/network-interface";
import { MomentModule } from "angular2-moment";
import { Globalization } from "@ionic-native/globalization";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/v4";

const firebaseConfig = {
  apiKey: "AIzaSyAJLShXOFgaBhNIZyGUw6rzBlzSTjttMfI",
  authDomain: "doveblaster-cdc83.firebaseapp.com",
  databaseURL: "https://doveblaster-cdc83.firebaseio.com",
  projectId: "doveblaster-cdc83",
  storageBucket: "doveblaster-cdc83.appspot.com",
  messagingSenderId: "763900904272",
  appId: "1:763900904272:web:fe68463f7ee257d8ef2255",
  measurementId: "G-63VKKWSGVM",

  // apiKey: "AIzaSyBLHQBV278t_1HW-ykisd7pbd2wsgLOkLI",
  // authDomain: "dayleasing-958cf.firebaseapp.com",
  // databaseURL: "https://dayleasing-958cf.firebaseio.com",
  // projectId: "dayleasing-958cf",
  // storageBucket: "dayleasing-958cf.appspot.com",
  // messagingSenderId: "674140232598",
  // appId: "1:674140232598:web:734a9bb71fad6c6d"

  /*   apiKey: "AIzaSyBb1oT_bAfXMsMHl1LI2hH5zNHVRNCP18E",
    authDomain: "testing-app-42ffd.firebaseapp.com",
    databaseURL: "https://testing-app-42ffd.firebaseio.com",
    projectId: "testing-app-42ffd",
    storageBucket: "",
    messagingSenderId: "760380489541",
    appId: "1:760380489541:web:090b439842911fbf" */
};
firebase.initializeApp(firebaseConfig);
@NgModule({
  declarations: [MyApp],
  imports: [
    MomentModule,
    OrderModule,
    CountdownModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AutoCompleteModule,
    PinchZoomModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AgmCoreModules.forRoot({
      apiKey: "AIzaSyA5Yt0rIC8x30xK2EonLDqjVDkrgcSoBFU",
      libraries: ["places", "drawing", "geometry"],
    }),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyA5Yt0rIC8x30xK2EonLDqjVDkrgcSoBFU",
      libraries: ["places", "drawing", "geometry"],
    }),
    AgmDirectionModule,
    IonicModule.forRoot(MyApp, {
      preloadModules: true,
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    Globalization,
    NetworkInterface,
    LocalNotifications,
    StatusBar,
    SplashScreen,
    Geolocation,
    SocialSharing,
    FileTransfer,
    FileTransferObject,
    File,
    FilePath,
    Camera,
    NativeGeocoder,
    NativePageTransitions,
    GoogleMapsAPIWrapper,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: FirestoreSettingsToken, useValue: {} },
    ApiProvider,
    ApicallsProvider,
    AuthProvider,
    DatePipe,
    Vibration,
    CompleteTestServiceProvider,
    GoogleMapsProvider,
    PayPal,
    FcmProvider,
    TracklocationProvider,
    FCM
  ],
})
export class AppModule {}
