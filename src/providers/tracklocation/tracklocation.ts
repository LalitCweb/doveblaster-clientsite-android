import { HttpClient } from "@angular/common/http";
import { Platform } from "ionic-angular";
import { Injectable } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";

declare var navigator: any;
@Injectable()
export class TracklocationProvider {
  constructor(
    public http: HttpClient,
    private geolocation: Geolocation,
    public auth: AuthProvider,
    public fcmp: FcmProvider,
    public platform: Platform
  ) {}
  lat: any;
  lng: any;
  watch: any;
  getlocation() {
    var tis = this;
    this.platform.ready().then(() => {
      // this.geolocation.onLocation(location => {
      //   localStorage.setItem(
      //     "currectlat",
      //     JSON.stringify(location.coords.latitude)
      //   );
      //   localStorage.setItem(
      //     "currectlng",
      //     JSON.stringify(location.coords.longitude)
      //   );
      // });
      // });
      // if (this.watch) {
      // } else {
      //   this.platform.ready().then(() => {
      //     // this.configureBackgroundGeolocation();
      //   });
      let options = {
        frequency: 3000,
        enableHighAccuracy: true
      };
      this.geolocation
        .getCurrentPosition(options)
        .then(resp => {
          tis.lat = resp.coords.latitude;
          tis.lng = resp.coords.longitude;
          localStorage.setItem(
            "currectlat",
            JSON.stringify(resp.coords.latitude)
          );
          localStorage.setItem(
            "currectlng",
            JSON.stringify(resp.coords.longitude)
          );
        })
        .catch(error => {
          this.auth.errtoast(error);
        });
      this.watch = this.geolocation.watchPosition(options);
      this.watch.subscribe(data => {
        // data can be a set of coordinates, or an error (if an error occurred).
        tis.lat = data.coords.latitude;
        tis.lng = data.coords.longitude;
        localStorage.setItem(
          "currectlat",
          JSON.stringify(data.coords.latitude)
        );
        localStorage.setItem(
          "currectlng",
          JSON.stringify(data.coords.longitude)
        );
      });
    });
  }

  getlat() {
    if (localStorage.getItem("currectlat")) {
      return JSON.parse(localStorage.getItem("currectlat"));
    }
  }
  getlng() {
    if (localStorage.getItem("currectlng")) {
      return JSON.parse(localStorage.getItem("currectlng"));
    }
  }

  stoplocation() {
    if (this.watch) {
      navigator.geolocation.clearWatch(this.watch);
    }
  }

  // configureBackgroundGeolocation() {
  //   let bgGeoFirebase = (<any>window).BackgroundGeolocationFirebase;

  //   bgGeoFirebase.configure({
  //     locationsCollection: "locations",
  //     geofencesCollection: "geofences"
  //   });

  //   let bgGeo = (<any>window).BackgroundGeolocation;

  //   bgGeo.on("location", location => {
  //     console.log("[location] - ", location);
  //   });

  //   bgGeo.ready(
  //     {
  //       stopOnTerminate: false,
  //       debug: true
  //     },
  //     state => {
  //       if (!state.enabled) {
  //         bgGeo.start();
  //       }
  //     }
  //   );
  // }
}
