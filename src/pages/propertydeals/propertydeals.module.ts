import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertydealsPage } from './propertydeals';

@NgModule({
  declarations: [
    PropertydealsPage,
  ],
  imports: [
    IonicPageModule.forChild(PropertydealsPage),
  ],
})
export class PropertydealsPageModule {}
