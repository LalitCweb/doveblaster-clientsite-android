import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import {
  PayPal,
  PayPalPayment,
  PayPalConfiguration
} from "@ionic-native/paypal";
// declare var paypal: any;

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-checkout",
  templateUrl: "checkout.html"
})
export class CheckoutPage {
  cartdata: any;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private payPal: PayPal,
    public alertCtrl: AlertController
  ) {
    this.cartdata = this.navParams.get("cartdata");
  }

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.auth.clearcookie();
  }
  havingdata: boolean = true;
  getCart() {
    this.auth.startloader();
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json() && data.json().status === 1) {
              if (this.cartdata && this.cartdata.length) {
                this.havingdata = true;
              } else {
                this.havingdata = false;
              }
              this.auth.clearcookie();
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
  }

  goto(page) {
    this.navCtrl.push(page);
  }
  carttotal: any;
  currency_symbol: any;
  subtotal: any;
  booking: any;
  loading: any;
  getcart() {
    this.loading = this.loadingCtrl.create({
      spinner: "crescent"
    });
    this.loading.present();
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(data => {
        this.loading.dismissAll();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            if (data.json().more_data) {
              this.cartdata.cart_data = data.json().more_data;
              this.cartdata.carttotal = data.json().total;
              // this.cartdata.booking =
              //   this.auth.convertToint(data.json().total) -
              //   data.json().content_total;
              this.cartdata.currency_symbol = data.json().currency_symbol;
              if (
                data.json().main_fees &&
                data.json().main_fees.booking_fee_4_95_slot &&
                data.json().main_fees.booking_fee_4_95_slot.amount
              ) {
                this.cartdata.subtotal =
                  data.json().content_total -
                  this.auth.convertToint(
                    data.json().main_fees.booking_fee_4_95_slot.amount
                  );
                this.cartdata.booking = this.auth.convertToint(
                  data.json().main_fees.booking_fee_4_95_slot.amount
                );
              }
              this.makePayment(this.cartdata.booking);
            }
          } else if (data.json() && data.json().status === 0) {
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (e) {
      this.auth.toast(e);
      this.loading.dismissAll();
    }
  }

  gotohome(page) {
    this.navCtrl.setRoot(page);
  }
  makePayment(conveniencefee) {
    this.loading = this.loadingCtrl.create({
      spinner: "crescent"
    });
    this.loading.present();
    try {
      this.auth.makepayment(conveniencefee).subscribe(data => {
        this.loading.dismissAll();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            if (this.auth.convertToint(this.cartdata.carttotal) < 1) {
              debugger;
              this.dontopenpaypal(data.json().order_id, this.auth.getuserId());
            } else {
              this.openpaypal(
                this.auth.convertToint(this.cartdata.carttotal),
                data.json().order_id,
                data.json().notify_url
              );
            }
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.loading.dismissAll();
      this.auth.errtoast(err);
    }
  }
  dontopenpaypal(order_id, user_id) {
    this.auth.startloader();
    try {
      this.auth
        .make_drct_rsrvtion_zero_paypl(order_id, user_id)
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json() && data.json().status === 1) {
              this.auth.clearcookie();
              this.getorder_detailzero(order_id);
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            } else if (data.json().status == 2) {
              // this.auth.logout();
              // this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  displaytimer: any = false;
  yourOwnFunction() {
    this.displaytimer = false;
    this.remove_getcart();
    this.showalert();
    this.auth.clearcookie();
    this.getCart();
  }
  showalert() {
    if(this.auth.ispaymentdone()){
      this.auth.paymentdone(false)
    }
    else{
      const alert = this.alertCtrl.create({
        title: "",
        subTitle:
          "Timer has expired and your cart has been emptied. Please re-add all slots to your cart and checkout before the timer expires.",
        buttons: ["OK"]
      });
      alert.present();
    }
  }
  remove_getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status === 1) {
              if (data.json().more_data && data.json().more_data.length) {
                for (var pro = 0; pro < data.json().more_data.length; pro++) {
                  // this.removeproductcart(data.json().more_data[pro], pro);
                  this.removeproductcart(
                    data.json().more_data[pro],
                    pro,
                    data.json().more_data.length - 1
                  );
                }
                // this.getCart();
                this.auth.clearcartstorage();
              }
              this.auth.clearcookie();
            } else {
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }

  removeproductcart(product, index, matchdata) {
    this.auth.startloader();
    try {
      this.auth
        .removeproductcart(this.auth.getuserId(), product.product_id)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.cartdata.splice(index, 1);
                this.carttotal = this.carttotal - parseFloat(product.price);
                this.auth.toast(data.json().message);
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
                this.getCart();
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            // this.auth.errtoast(errorHandler);
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  // functionPaypal(payment_amount, order_id, notify_url) {
  //   debugger;
  //   console.log(order_id);
  //   // document.getElementById("paypal-button-container").style.display =
  //   //   "inline-block";
  //   // Render the PayPal button into #paypal-button-container
  //   var el = this;
  //   paypal
  //     .Buttons({
  //       // Set up the transaction
  //       createOrder: function(data, actions) {
  //         //alert(data);
  //         return actions.order.create({
  //           purchase_units: [
  //             {
  //               amount: {
  //                 value: payment_amount.toString()
  //               }
  //             }
  //           ]
  //         });
  //       },

  //       // Finalize the transaction
  //       onApprove: function(data, actions) {
  //         return actions.order.capture().then(function(details) {
  //           debugger;
  //           var response = {
  //             id: details.id,
  //             state: details.status,
  //             create_time: details.create_time,
  //             intent: details.intent
  //           };
  //           el.getorder_detail(order_id, response, "Paypal");
  //           console.log(details);
  //           return fetch("/paypal-transaction-complete", {
  //             method: "post",
  //             headers: {
  //               "content-type": "application/json"
  //             },
  //             body: JSON.stringify({
  //               order_id: order_id,
  //               notify_url: notify_url
  //             })
  //           });
  //         });
  //       }
  //     })
  //     .render("#paypal-button-container");
  // }

  openpaypal(payment_amount, order_id, notify_url) {
    this.payPal
      .init({
        PayPalEnvironmentProduction:
          "AXcsrflk0GEELkHR72qpqUJwqxTJV4q1pkqdzFGdjKpWAnhvidC62cs4yLUrav4nEa6LCu9rOstcjknW",
        // "ASJnB8cSt5ruW4vvcwpr-4x1OLC01gxB3yYR8qyZGeg1GMEw3DRdsW9hxZmOp5hQ7laYIUdSTS4Vk09R",
        PayPalEnvironmentSandbox:
          // "AeTvucZPJ6JsRlV3LQRnXyfMHASp3sVBcW6oABSfXZqsxnoUCmqsOgxrl00l7SXhtKOD_Z9vN-YYN_UB"
          "AdAC1DHrZ1PXZdgews9TNlQuBSspYskhxQ3Nc_y1sGX7u1UPFp5acll2Z2W1A6GpNj_6v5NMC7YIIcO_"
        // "AVtu8I78hTz7ZygVLlglLor2t0_erFosZgtHwr9d6Thgpg68v3xcIQTAAa6fqpcEQzNoOutZV9DtXpqL"
      })
      .then(
        env => {
          // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
          this.payPal
            .prepareToRender(
              "PayPalEnvironmentProduction",
              new PayPalConfiguration({
                // Only needed if you get an "Internal Service Error" after PayPal login!
                //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
              })
            )
            .then(
              pay => {
                debugger;
                let payment = new PayPalPayment(
                  payment_amount.toString(),
                  "USD",
                  "Description",
                  "sale"
                );
                payment.custom = JSON.stringify({
                  order_id: order_id,
                  notify_url: notify_url
                });
                this.payPal.renderSinglePaymentUI(payment).then(
                  response => {
                    debugger;
                    if (response.response.state == "approved") {
                      this.auth.clearcookie();
                      this.getorder_detail(order_id, response, "Paypal");
                      this.auth.paymentdone(true)
                    }
                    // Successfully paid
                    // Example sandbox response
                    //
                    // {
                    //   "client": {
                    //     "environment": "sandbox",
                    //     "product_name": "PayPal iOS SDK",
                    //     "paypal_sdk_version": "2.16.0",
                    //     "platform": "iOS"
                    //   },
                    //   "response_type": "payment",
                    //   "response": {
                    //     "id": "PAY-1AB23456CD789012EF34GHIJ",
                    //     "state": "approved",
                    //     "create_time": "2016-10-03T13:33:33Z",
                    //     "intent": "sale"
                    //   }
                    // }
                  },
                  () => {
                    // Error or render dialog closed without being successful
                  }
                );
              },
              () => {
                // Error in configuration
              }
            );
        },
        () => {
          // Error in initialization, maybe PayPal isn't supported or something else
        }
      );
  }

  getorder_detail(order_id, response, paymentmethod) {
    debugger;
    // var ee: any = this;
    this.auth.startloader();
    try {
      this.auth.orderdetail(order_id).subscribe(data => {
        var ordt: any = data.json()
        this.auth.stoploader();
        if (data) {
          if (ordt && ordt.status === 1) {
            this.auth.clearcookie();

            this.cartdata.cart_data.forEach((iten, index) => {
              debugger;
              this.confirmorder(iten, response, order_id);
            });
            this.navCtrl.setRoot("OrdersuccessPage", {
              order_detail: ordt.order_data[0],
              cart_data: this.cartdata.cart_data,
              paymentmethod: paymentmethod
            });
          } else if (ordt && ordt.status === 0) {
            this.auth.clearcookie();
            this.auth.toast(ordt.message);
          } else if (ordt.status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  getorder_detailzero(order_id) {
    this.auth.startloader();
    try {
      this.auth.orderdetail(order_id).subscribe(data => {
        this.auth.stoploader();
        if (data) {
          if (data.json() && data.json().status === 1) {
            this.auth.clearcookie();
            this.navCtrl.setRoot("OrdersuccessPage", {
              order_detail: data.json().order_data[0],
              cart_data: this.cartdata.cart_data
            });
          } else if (data.json() && data.json().status === 0) {
            this.auth.clearcookie();
            this.auth.toast(data.json().message);
          } else if (data.json().status == 2) {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  confirmorder(data, response, order_id) {
    this.auth.startloader();
    try {
      var ddt: any = {
        property_id: data.property_id,
        fromdate: data.date_of_product,
        to_date: data.date_of_product,
        linked_slot_tobooking: data.slotid,
        product_price: data.price,
        booking_product_price_after_discount: data.price_after_discount,
        product_id: data.product_id,
        order_id: order_id,
        transaction_id: response.response.id,
        hunter_paypal_email: this.auth.getUsername("email"),
        user_id: this.auth.getuserId()
      };
      debugger;
      this.auth.confirmorder(ddt).subscribe(datas => {
        debugger;
        var conord: any = datas.json()
        this.auth.stoploader();
        if (data) {
          if (conord && conord.status === 1) {
            this.auth.clearcookie();
          } else if (conord && conord.status === 0) {
            this.auth.clearcookie();
            this.auth.toast(conord.message);
          } else if (conord.status == 2) {
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          }
        }
      });
    } catch (err) {
      this.auth.clearcookie();
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
}
