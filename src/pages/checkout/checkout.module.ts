import {
  NgModule,
  NO_ERRORS_SCHEMA,
  CUSTOM_ELEMENTS_SCHEMA
} from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CheckoutPage } from "./checkout";
import { CountdownModule } from "ngx-countdown";

@NgModule({
  declarations: [CheckoutPage],
  imports: [IonicPageModule.forChild(CheckoutPage), CountdownModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CheckoutPageModule {}
