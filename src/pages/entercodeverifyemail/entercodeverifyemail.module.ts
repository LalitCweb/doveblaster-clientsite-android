import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { EntercodeverifyemailPage } from "./entercodeverifyemail";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [EntercodeverifyemailPage],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(EntercodeverifyemailPage)
  ]
})
export class EntercodeverifyemailPageModule {}
