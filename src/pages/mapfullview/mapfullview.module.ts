import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MapfullviewPage } from "./mapfullview";
import { ComponentsModule } from "../../components/components.module";
import { AgmCoreModules } from "../../agm/core";
import { MyDatePickerModule } from "mydatepicker";

@NgModule({
  declarations: [MapfullviewPage],
  imports: [
    MyDatePickerModule,
    ComponentsModule,
    AgmCoreModules,
    IonicPageModule.forChild(MapfullviewPage)
  ]
})
export class MapfullviewPageModule {}
