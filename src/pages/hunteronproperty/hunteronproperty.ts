import {
  Component,
  ViewChild,
  ElementRef,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  AlertController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";
import { AngularFireDatabase } from "angularfire2/database";
import { Observable } from "rxjs";
import { Geolocation } from "@ionic-native/geolocation";
// import * as firebase from "Firebase";
import * as firebase from "firebase/app";

import { DatePipe } from "@angular/common";
import { AngularFirestore, QuerySnapshot } from "angularfire2/firestore";
import { combineLatest } from "rxjs/observable/combineLatest";
import { map, filter, catchError, mergeMap } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-hunteronproperty",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "hunteronproperty.html"
})
export class HunteronpropertyPage {
  @ViewChild("map_canvas") mapElements: ElementRef;
  options: any = {
    zoom: 16,
    lat: 0,
    lng: 0,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 3,
    strokePos: 0,
    image: "assets/images/red.png"
  };
  devices: any;
  check: any;
  checkSubscribe: any;
  @ViewChild("map") mapElement: ElementRef;
  map: any;
  ref = firebase.database().ref("/doveblaster_data/");
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public fcmp: FcmProvider,
    public afd: AngularFireDatabase,
    public platform: Platform,
    public datepipe: DatePipe,
    public alertCtrl: AlertController,
    public afs: AngularFirestore
  ) {
    if (this.auth.isOwner()) {
      this.userbookings = [];
      var property_id: any;
      if (this.navParams.get("property_detail")) {
        property_id = this.navParams.get("property_detail").property_id;
        this.subscribeagain(property_id);
        this.propertydetail(
          property_id,
          this.datepipe.transform(new Date(), "yyyy-MM-dd"),
          true
        );
        // this.userreservation();
        var tis = this;
        // this.trackhunters = setInterval(() => {
        //   tis.slotdata = [];
        //   tis.propertydetail(
        //     property_id,
        //     tis.datepipe.transform(new Date(), "yyyy-MM-dd"),
        //     false
        //   );
        //   tis.userreservation();
        // }, 900000);

        // this.refreshusers = setInterval(() => {
        //   var tis = this;
        //   tis.slotdata = [];
        //   // Clear Users
        //   for (var x = 0; x < tis.slotdata.length; x++) {
        //     tis.slotdata[x] = [];
        //     for (var y = 0; y < tis.slotdatasubscriptions.length; y++) {
        //       tis.slotdatasubscriptions[x].unsubscribe();
        //     }
        //   }
        //   // Reassigning Users
        //   if (this.upcoming && this.upcoming.length) {
        //     this.userbookings = [];
        //     for (var x = 0; x < this.upcoming.length; x++) {
        //       if (
        //         this.upcoming[x].property_id ==
        //         this.navParams.get("property_detail").property_id
        //       ) {
        //         this.userbookings.push(this.upcoming[x].author_id);
        //       }
        //       if (this.upcoming.length - 1 == x) {
        //         this.gettrackdata(this.userbookings);
        //       }
        //     }
        //   }
        // }, 3000);
      }
    }

    platform.ready().then(() => {
      this.initMap();
    });
  }
  reload() {
    var tis = this;
    tis.slotdata = [];
    tis.upcoming = [];
    this.userbookings = [];
    for (var x = 0; x < tis.slotdata.length; x++) {
      tis.slotdata[x] = [];
      tis.slotdata[x] = [];
      for (var y = 0; y < tis.slotdatasubscriptions.length; y++) {
        tis.slotdatasubscriptions[x].unsubscribe();
      }
    }
    tis.propertydetail(
      tis.navParams.get("property_detail").property_id,
      tis.datepipe.transform(new Date(), "yyyy-MM-dd"),
      false
    );
    tis.userreservation();
  }
  trackhunters: any;
  refreshusers: any;
  slotdata: any = [];
  slotdatasubscriptions: any = [];
  gettrackdata(users) {
    this.slotdata = [];
    var combinedList: any;
    var tis = this;
    if (users && users.length) {
      debugger;
      for (var x = 0; x < users.length; x++) {
        this.slotdata[x] = this.afd
          .list("/dayleasingdata", ref =>
            ref
              .orderByChild("userid")
              .equalTo(users[x])
              .limitToLast(1)
          )
          .valueChanges();
        this.slotdatasubscriptions[x] = this.slotdata[
          x
        ].subscribe(queriedItems => {});
        // Clear Users
        // for (var x = 0; x < tis.slotdata.length; x++) {
        //   tis.slotdata[x] = [];
        //   for (var y = 0; y < tis.slotdatasubscriptions.length; y++) {
        //     tis.slotdatasubscriptions[x].unsubscribe();
        //   }
        // }
      }
    }
  }
  checktime2(marker) {
    var diffDays = new Date().getTime() - marker;
    if (diffDays > 10000) {
      return false;
    } else {
      return true;
    }
  }
  clearData() {
    this.afd.list("/").remove();
  }

  devicesSubscribe: any;
  subscribeagain(property_id) {
    var dd: any = this.fcmp.checkproperty(property_id).valueChanges();
    var ee = this;
    dd.subscribe(items => {
      ee.devices = items;
      // console.log(ee.devices);
    });
  }
  ionViewWillEnter() {}
  ionViewDidLoad() {}
  ionViewWillLeave() {
    clearInterval(this.trackhunters);
    clearInterval(this.refreshusers);
    if (this.checkSubscribe) {
      this.checkSubscribe.unsubscribe();
    }
    if (this.devicesSubscribe) {
      this.devicesSubscribe.unsubscribe();
    }
  }
  //////////////////////////////////
  ///////////Check time/////////////
  /////////////////////////////////
  checktime1(marker) {
    console.log(marker);
    var timeDiff = Math.abs(new Date().getTime() - Date.parse(marker.datetime));
    var diffDays = Math.ceil(timeDiff / 1000);
    // console.log(timeDiff);
    // console.log(diffDays);
    if (diffDays < 180) {
      return true;
    } else {
      return false;
    }
  }
  checktime(marker) {
    console.log(
      Date.parse(
        new Date().toLocaleString("en-US", { timeZone: marker.iana_timezone })
      )
    );
    console.log(Date.parse(marker.datetime));
    var timeDiff = Math.abs(
      Date.parse(
        new Date().toLocaleString("en-US", { timeZone: marker.iana_timezone })
      ) - Date.parse(marker.datetime)
    );
    var diffDays = Math.ceil(timeDiff / 1000);
    if (diffDays < 60) {
      return true;
    } else {
      return false;
    }
  }

  ///////////////////////////
  ///////////Map/////////////
  ///////////////////////////
  markers = [];
  initMap() {}

  addMarker(location) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: "assets/images/red.png"
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  updateGeolocation(lat, lng) {}

  /******************/
  /***Touch Events***/
  /******************/
  touchstart(e) {
    if (e.cancelable) {
      e.preventDefault();
    }
  }
  ////////////////////////////////
  ////////Property Detail/////////
  ///////////////////////////////
  slotarea: any;
  option: any;
  propertydetail(property, date, loader) {
    date = this.datepipe.transform(date, "yyyy-MM-dd");
    if (loader) {
      this.auth.startloader();
    }
    try {
      this.auth.getsinglealldetail(property, this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (loader) {
              this.auth.stoploader();
            }
            if (data.json().status == 1) {
              this.slotarea = data.json().events;
              this.options.lat = this.auth.convertToint(
                data.json().events.prop_area_center_lat
              );
              this.options.lng = this.auth.convertToint(
                data.json().events.prop_area_center_lng
              );
              var cords: any = {};
              if (this.slotarea.prop_area_shape_type == "polygon") {
                this.slotarea.prop_area_shape_cords = this.slotarea.prop_area_shape_cords.replace(
                  /\\/g,
                  ""
                );
                this.slotarea.prop_area_shape_cords = JSON.parse(
                  this.slotarea.prop_area_shape_cords
                );
                if (
                  this.slotarea.prop_area_shape_cords &&
                  this.slotarea.prop_area_shape_cords.length
                ) {
                  for (
                    var z = 0;
                    z < this.slotarea.prop_area_shape_cords.length;
                    z++
                  ) {
                    this.slotarea.prop_area_shape_cords[
                      z
                    ] = this.slotarea.prop_area_shape_cords[z].split(",");
                    cords = {
                      lat: parseFloat(
                        this.slotarea.prop_area_shape_cords[z][0]
                      ),
                      lng: parseFloat(this.slotarea.prop_area_shape_cords[z][1])
                    };
                    if (z == 0) {
                      this.slotarea.prop_area_shape_cord = [];
                    }
                    this.slotarea.prop_area_shape_cord.push(cords);
                  }
                }
              }

              /////////////////////////
              /////////Slots///////////
              /////////////////////////
              for (
                var slts = 0;
                slts < this.slotarea.all_areas.length;
                slts++
              ) {
                if (
                  this.slotarea.all_areas[slts].slots_full_details &&
                  this.slotarea.all_areas[slts].slots_full_details.length
                ) {
                  for (
                    var slt = 0;
                    slt <
                    this.slotarea.all_areas[slts].slots_full_details.length;
                    slt++
                  ) {
                    var cords: any = {};
                    if (
                      this.slotarea.all_areas[slts].slots_full_details[slt]
                        .shapetype == "polygon"
                    ) {
                      this.slotarea.all_areas[slts].slots_full_details[
                        slt
                      ].slot_coordinates = this.slotarea.all_areas[
                        slts
                      ].slots_full_details[slt].slot_coordinates.replace(
                        /\\/g,
                        ""
                      );
                      this.slotarea.all_areas[slts].slots_full_details[
                        slt
                      ].slot_coordinates = JSON.parse(
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates
                      );
                      if (
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates &&
                        this.slotarea.all_areas[slts].slots_full_details[slt]
                          .slot_coordinates.length
                      ) {
                        for (
                          var z = 0;
                          z <
                          this.slotarea.all_areas[slts].slots_full_details[slt]
                            .slot_coordinates.length;
                          z++
                        ) {
                          this.slotarea.all_areas[slts].slots_full_details[
                            slt
                          ].slot_coordinates[z] = this.slotarea.all_areas[
                            slts
                          ].slots_full_details[slt].slot_coordinates[z].split(
                            ","
                          );
                          cords = {
                            lat: parseFloat(
                              this.slotarea.all_areas[slts].slots_full_details[
                                slt
                              ].slot_coordinates[z][0]
                            ),
                            lng: parseFloat(
                              this.slotarea.all_areas[slts].slots_full_details[
                                slt
                              ].slot_coordinates[z][1]
                            )
                          };
                          if (z == 0) {
                            this.slotarea.all_areas[slts].slots_full_details[
                              slt
                            ].slot_coordinate = [];
                          }
                          this.slotarea.all_areas[slts].slots_full_details[
                            slt
                          ].slot_coordinate.push(cords);
                        }
                      }
                    }
                  }
                }
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          if (loader) {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
          }
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (err) {
      if (loader) {
        this.auth.stoploader();
      }
      this.auth.errtoast(err);
    }
  }

  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    map.setOptions({
      zoomControl: "false",
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT
      }
    });
  }
  getoken(token) {
    this.sendtoken(token);
  }
  sendtokenall() {
    const prompt = this.alertCtrl.create({
      title: "Notification",
      message: "Send message to hunters",
      inputs: [
        {
          name: "body",
          placeholder: "Message"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {}
        },
        {
          text: "Send",
          handler: data => {
            if (data.body) {
              var tkn = this;
              var dev: any = this.fcmp
                .checkproperty(
                  this.navParams.get("property_detail").property_id
                )
                .valueChanges();
              var devsubs = dev.subscribe(queriedItems => {
                var userst: any = [];
                var users: any = [];
                queriedItems.forEach(function(tokens, index) {
                  if (userst.includes(tokens.token)) {
                  } else {
                    users.push(tokens);
                    userst.push(tokens.token);
                  }
                });
                users.forEach(function(tokens, index) {
                  if (tkn.checktime(tokens)) {
                    tkn.sendNotificationall(
                      queriedItems.length,
                      index,
                      tokens,
                      data.body
                    );
                  }
                  if (index == users.length - 1) {
                    if (devsubs) {
                      devsubs.unsubscribe();
                    }
                  }
                });
              });
            } else {
              this.auth.toast("Please type your message");
            }
          }
        }
      ]
    });
    prompt.present();
  }
  selecteduser: any = {};
  sendtoken(token) {
    this.selecteduser.token = token.token;
    const prompt = this.alertCtrl.create({
      title: "Notification",
      message: "Send message to " + token.userName,
      inputs: [
        {
          name: "body",
          placeholder: "Message"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {}
        },
        {
          text: "Send",
          handler: data => {
            if (data.body) {
              this.sendNotification(data.body);
            } else {
              this.auth.toast("Please type your message");
            }
          }
        }
      ]
    });
    prompt.present();
  }
  title: any;
  body: any;
  sendNotification(body) {
    this.auth.startloader();
    try {
      this.auth.sendpushnotification(this.selecteduser.token, body).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            this.title = "";
            this.body = "";
            var dataa: any = data.json();
            if (dataa.success == 1) {
              this.auth.toast("Notification send successfully");
            } else {
              this.auth.toast("Not able to send notification");
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  sendNotificationall(item, index, token, body) {
    if (body) {
      this.auth.startloader();
      try {
        this.auth.sendpushnotification(token.token, body).subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              this.title = "";
              this.body = "";
              var dataa: any = data.json();
              if (
                (dataa.success == 1 && index == item - 1) ||
                (dataa.success == 1 && index == 0)
              ) {
                this.auth.toast("Notification send successfully");
              } else if (index == item - 1) {
                this.auth.toast("Not able to send notification");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
          }
        );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else {
      this.auth.toast("Please type your message");
    }
  }

  cancelnotification() {
    this.selecteduser = {};
  }

  userbookings: any = [];
  upcoming: any;
  userreservation() {
    try {
      this.auth.userreservation(this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json().status == 1) {
              this.upcoming = data.json().upcoming_bookings;
              if (this.upcoming && this.upcoming.length) {
                this.userbookings = [];
                for (var x = 0; x < this.upcoming.length; x++) {
                  if (
                    this.upcoming[x].property_id ==
                    this.navParams.get("property_detail").property_id
                  ) {
                    this.userbookings.push(this.upcoming[x].author_id);
                  }
                  if (this.upcoming.length - 1 == x) {
                    this.gettrackdata(this.userbookings);
                    this.getpropertydetail(
                      this.navParams.get("property_detail").property_id
                    );
                  }
                }
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  productdetail: any;
  getpropertydetail(marker) {
    try {
      this.auth.getsinglealldetail(marker, this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data && data.json() && data.json().status == 1) {
              this.productdetail = data.json().events;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          // this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      // this.auth.toast(e);
    }
  }
  checkuserinside(coords) {
    // this.productdetail.prop_area_shape_type
    var el = this;
    //////////////////////////////////////
    ////////Property Outline /////////////
    /////////////////////////////////////
    if (coords && this.productdetail) {
      if (el.productdetail.prop_area_shape_type == "polygon") {
        if (typeof el.productdetail.prop_area_shape_cords == "string") {
          el.productdetail.prop_area_shape_cords = JSON.parse(
            el.productdetail.prop_area_shape_cords
          );
          ///////////////////////////////////
          ////////Change Coordinate/////////
          //////////////////////////////////
          var cords: any = {};
          el.productdetail.prop_area_shape_cord = [];
          for (
            var x = 0;
            x < el.productdetail.prop_area_shape_cords.length;
            x++
          ) {
            try {
              el.productdetail.prop_area_shape_cords[
                x
              ] = el.productdetail.prop_area_shape_cords[x].split(",");
            } catch (e) {
              // return;
            }
            cords = {
              lat: parseFloat(el.productdetail.prop_area_shape_cords[x][0]),
              lng: parseFloat(el.productdetail.prop_area_shape_cords[x][1])
            };
            el.productdetail.prop_area_shape_cord.push(cords);
            el.productdetail.prop_area_shape_cords[x].lat =
              el.productdetail.prop_area_shape_cords[x][0];
            el.productdetail.prop_area_shape_cords[x].lng =
              el.productdetail.prop_area_shape_cords[x][1];
          }
          var notify: any = el.polygonlocation(
            el.productdetail.prop_area_shape_cord,
            coords.lat,
            coords.lng
          );
          if (notify && el.auth.isOwner()) {
            //
            // Update code here
            //
            return true;
          } else {
            //
            // Update code here
            //
            return false;
          }
        } else {
          var notify: any = el.polygonlocation(
            el.productdetail.prop_area_shape_cord,
            coords.lat,
            coords.lng
          );
          if (notify && el.auth.isOwner()) {
            //
            // Update code here
            //
            return true;
          } else {
            //
            // Update code here
            //
            return false;
          }
        }
      } else if (el.productdetail.prop_area_shape_type == "rectangle") {
        if (typeof el.productdetail.prop_area_shape_cords == "string") {
          el.productdetail.prop_area_shape_cords = JSON.parse(
            el.productdetail.prop_area_shape_cords
          );
          var notify: any = el.rectanglelocation(
            el.productdetail.prop_area_shape_cords,
            coords.lat,
            coords.lng
          );
          if (notify && el.auth.isOwner()) {
            //
            // Update code here
            //
            return true;
          } else {
            //
            // Update code here
            //
            return false;
          }
        } else {
          var notify: any = el.rectanglelocation(
            el.productdetail.prop_area_shape_cords,
            coords.lat,
            coords.lng
          );
          if (notify && el.auth.isOwner() == false) {
            //
            // Update code here
            //
            return true;
          } else {
            //
            // Update code here
            //
            return false;
          }
        }
      }
    }
  }

  polygonlocation(reservation, currentlat, currentlng) {
    if (reservation && currentlat && currentlng) {
      return google.maps.geometry.poly.containsLocation(
        new google.maps.LatLng(currentlat, currentlng),
        new google.maps.Polygon({
          paths: reservation
        })
      )
        ? true
        : false;
    } else {
      return false;
    }
  }

  map_canvas: any;
  rectanglelocation(reservation, currentlat, currentlng) {
    if (reservation && currentlat && currentlng) {
      var rectangle;
      rectangle = new google.maps.Rectangle({
        bounds: reservation[0],
        editable: true,
        draggable: true,
        geodesic: true,
        map: this.map_canvas
      });
      return rectangle.getBounds().contains({
        lat: currentlat,
        lng: currentlng
      });
    } else {
      return false;
    }
  }
}
