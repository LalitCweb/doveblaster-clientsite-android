import {
  NgModule,
  NO_ERRORS_SCHEMA,
  CUSTOM_ELEMENTS_SCHEMA
} from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MycartPage } from "./mycart";
import { CountdownModule } from "ngx-countdown";

@NgModule({
  declarations: [MycartPage],
  imports: [IonicPageModule.forChild(MycartPage), CountdownModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class MycartPageModule {}
