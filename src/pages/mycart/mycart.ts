import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Platform
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { DatePipe } from "@angular/common";
import { CountdownComponent } from "ngx-countdown";
/**
 * Generated class for the MycartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-mycart",
  templateUrl: "mycart.html"
})
export class MycartPage {
  @ViewChild("countdown") counter: CountdownComponent;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public alertController: AlertController,
    public datepipe: DatePipe,
    public alertCtrl: AlertController,
    public platform: Platform
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.auth.clearcookie();
    this.getCart();
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
    this.counter.stop();
  }

  goto(page, cart_data, subtotal, carttotal, booking, currency_symbol) {
    var data: any = {
      cart_data: cart_data,
      subtotal: subtotal,
      carttotal: carttotal,
      booking: booking,
      currency_symbol: currency_symbol
    };
    this.navCtrl.push(page, { cartdata: data });
  }

  gotohome(page) {
    this.navCtrl.setRoot(page);
  }
  cartdata: any = [];
  carttotal: any;
  currency_symbol: any;
  subtotal: any;
  booking: any;
  coupondata: any = {};
  getCart() {
    this.auth.startloader();
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json() && data.json().status === 1) {
              this.cartdata = data.json().more_data;
              if (
                data.json().main_fees &&
                data.json().main_fees.booking_fee_4_95_slot &&
                data.json().main_fees.booking_fee_4_95_slot.amount
              ) {
                this.subtotal =
                  data.json().content_total -
                  this.auth.convertToint(
                    data.json().main_fees.booking_fee_4_95_slot.amount
                  );
              } else {
                this.subtotal = data.json().content_total;
              }

              if (this.cartdata && this.cartdata.length) {
                this.coupondata.applied_coupons_price = 0;
                for (var crtdt = 0; crtdt < this.cartdata.length; crtdt++) {
                  this.coupondata.applied_coupons_price =
                    this.coupondata.applied_coupons_price +
                    this.auth.convertToint(this.cartdata[crtdt].price);
                }
                this.coupondata.applied_coupons_price =
                  this.coupondata.applied_coupons_price - this.subtotal;
              }
              this.carttotal = data.json().total;
              if (
                data.json().main_fees &&
                data.json().main_fees.booking_fee_4_95_slot &&
                data.json().main_fees.booking_fee_4_95_slot.amount
              ) {
                this.booking = this.auth.convertToint(
                  data.json().main_fees.booking_fee_4_95_slot.amount
                );
              } else {
                this.booking = 0;
              }
              // this.booking =
              //   this.auth.convertToint(data.json().total) -
              //   data.json().content_total;
              this.currency_symbol = data.json().currency_symbol;
              if (data.json().applied_coupons) {
                this.coupondata.applied_coupons = data.json().applied_coupons;
              }
              if (this.cartdata && this.cartdata.length) {
                setTimeout(() => this.counter.restart());
                this.startimers(this.auth.getcarttime());
              }
              this.auth.clearcookie();
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  /*************************/
  /*****Remove Property*****/
  /*************************/

  async presentAlertConfirm(product, index) {
    const alert = await this.alertController.create({
      message: "Are you sure you want to delete this property from cart ?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.removeproductcart(product, index, null);
          }
        }
      ]
    });
    await alert.present();
  }

  removeproductcart(product, index, matchdata) {
    this.auth.startloader();
    try {
      this.auth
        .removeproductcart(this.auth.getuserId(), product.product_id)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.cartdata.splice(index, 1);
                this.carttotal = this.carttotal - parseFloat(product.price);
                this.auth.toast(data.json().message);
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
                this.getCart();
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            // this.auth.errtoast(errorHandler);
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  applycoupon(coupon) {
    if (coupon) {
      try {
        this.auth.applycoupon(this.auth.getuserId(), coupon).subscribe(
          data => {
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.auth.toast(data.json().message);
                this.getCart();
              } else if (data.json() && data.json().status === 0) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                } else {
                  this.auth.toast(
                    "Coupon is not suitable for these cart items"
                  );
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }

  removecoupon(coupon) {
    if (coupon) {
      try {
        this.auth.removecouponcart(this.auth.getuserId(), coupon).subscribe(
          data => {
            if (data) {
              if (data.json() && data.json().status === 1) {
                this.auth.toast(data.json().message);
                this.getCart();
              } else if (data.json() && data.json().status === 0) {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                } else {
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
      } catch (e) {
        this.auth.stoploader();
        this.auth.toast(e);
      }
    }
  }
  // timerdatetime: any;
  startimer(datetime) {
    this.auth.addedtocart(datetime);
    this.triggertimer(datetime);
  }
  triggertimer(datetime) {
    this.startimers(datetime);
  }
  changeworld() {
    this.startimer(new Date());
  }
  ////////////////////////////////
  ///////////Timer////////////////
  ///////////////////////////////
  endatetime: any;
  displaytimer: any = false;
  public startimers(dates) {
    this.displaytimer = true;
    var setdate = new Date(dates);
    var myTimeSpan = 5 * 60 * 1000;
    var currentdate = setdate.setTime(setdate.getTime() + myTimeSpan);

    if (this.platform.is("ios")) {
      let datePipe = new DatePipe("en-US");
      this.endatetime = datePipe.transform(
        currentdate,
        "yyyy-MM-dd HH:mm:ss",
        "+0000"
      );
      this.endatetime = this.endatetime.replace(/\s/, "T");
      this.endatetime =
        new Date(this.endatetime).getTime() / 1000 -
        new Date().getTime() / 1000;
      if (this.endatetime < 0) {
        this.displaytimer = false;
        this.remove_getcart();
      }
    } else {
      this.endatetime = this.datepipe.transform(
        currentdate,
        "yyyy-MM-dd HH:mm:ss"
      );
      this.endatetime = this.endatetime.replace(/\s/, "T");
      this.endatetime =
        new Date(this.endatetime).getTime() / 1000 -
        new Date().getTime() / 1000;
      if (this.endatetime < 0) {
        this.displaytimer = false;
        this.remove_getcart();
      }
    }
  }

  yourOwnFunction() {
    this.displaytimer = false;
    this.remove_getcart();
    this.showalert();
    this.auth.clearcookie();
    this.getCart();
  }
  showalert() {
    if(this.auth.ispaymentdone()){
      this.auth.paymentdone(false)
    }
    else{
      const alert = this.alertCtrl.create({
        title: "",
        subTitle:
          "Timer has expired and your cart has been emptied. Please re-add all slots to your cart and checkout before the timer expires.",
        buttons: ["OK"]
      });
      alert.present();
    }
  }
  remove_getcart() {
    try {
      this.auth.getCart(this.auth.getuserId()).subscribe(
        data => {
          if (data) {
            if (data.json() && data.json().status === 1) {
              if (data.json().more_data && data.json().more_data.length) {
                for (var pro = 0; pro < data.json().more_data.length; pro++) {
                  // this.removeproductcart(data.json().more_data[pro], pro);
                  this.removeproductcart(
                    data.json().more_data[pro],
                    pro,
                    data.json().more_data.length - 1
                  );
                }
                // this.getCart();
                this.auth.clearcartstorage();
              }
              this.auth.clearcookie();
            } else {
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.clearcookie();
      this.auth.toast(e);
    }
  }
}
