import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddnewdatePage } from "./addnewdate";
import { MyDatePickerModule } from "mydatepicker";
@NgModule({
  declarations: [AddnewdatePage],
  imports: [MyDatePickerModule, IonicPageModule.forChild(AddnewdatePage)]
})
export class AddnewdatePageModule {}
