import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddareainfoPage } from "./addareainfo";
import { MyDatePickerModule } from "mydatepicker";
@NgModule({
  declarations: [AddareainfoPage],
  imports: [MyDatePickerModule, IonicPageModule.forChild(AddareainfoPage)]
})
export class AddareainfoPageModule {}
