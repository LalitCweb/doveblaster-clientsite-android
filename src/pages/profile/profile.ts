import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  Platform,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  ActionSheetController
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { PopoverController } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Camera } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";
import { HttpClient } from "@angular/common/http";
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage implements OnInit {
  disabled: any = true;
  configs: FormGroup;
  constructor(
    public Platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public auth: AuthProvider,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    private filePath: FilePath,
    private file: File,
    public http: HttpClient
  ) {}

  ionViewWillEnter() {
    this.getuserdata(JSON.parse(this.auth.getUser2()).username);
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
  }

  ionViewDidLoad() {}
  ngOnInit() {
    let EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.configs = new FormGroup({
      fname: new FormControl("", [Validators.required]),
      lname: new FormControl("", [Validators.required]),
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(EMAILPATTERNS)
      ]),
      phone: new FormControl(""),
      mphone: new FormControl("", [Validators.required]),
      ppayment: new FormControl("", [Validators.pattern(EMAILPATTERNS)]),
      about: new FormControl(""),
      live: new FormControl(""),
      speak: new FormControl(""),
      facebook: new FormControl(""),
      twitter: new FormControl("")
    });
  }
  more() {
    if (this.disabled == true) {
      this.disabled = false;
    } else {
      this.disabled = true;
    }
  }

  data: any = {
    first_name: "",
    last_name: "",
    phone: "",
    mobile: "",
    paypal_payments_to: "",
    description: "",
    live_in: "",
    i_speak: "",
    facebook: "",
    twitter: "",
    user_email: ""
  };
  getuserdata(user_id) {
    this.auth.clearcookie();
    this.auth.startloader();
    try {
      this.auth.getuserdata(user_id).subscribe(
        data => {
          this.auth.clearcookie();
          this.auth.stoploader();
          if (data) {
            var dataa: any = data.json().posts;
            // this.data = dataa;
            if (dataa.first_name && dataa.first_name[0]) {
              this.data.first_name = dataa.first_name[0];
            }
            if (dataa.last_name && dataa.last_name[0]) {
              this.data.last_name = dataa.last_name[0];
            }
            if (dataa.phone && dataa.phone[0]) {
              this.data.phone = dataa.phone[0];
            }
            if (dataa.mobile && dataa.mobile[0]) {
              this.data.mobile = dataa.mobile[0];
            }
            if (dataa.paypal_payments_to && dataa.paypal_payments_to[0]) {
              this.data.paypal_payments_to = dataa.paypal_payments_to[0];
            }
            if (dataa.description && dataa.description[0]) {
              this.data.description = dataa.description[0];
            }
            if (dataa.live_in && dataa.live_in[0]) {
              this.data.live_in = dataa.live_in[0];
            }
            if (dataa.i_speak && dataa.i_speak[0]) {
              this.data.i_speak = dataa.i_speak[0];
            }
            if (dataa.facebook && dataa.facebook[0]) {
              this.data.facebook = dataa.facebook[0];
            }
            if (dataa.twitter && dataa.twitter[0]) {
              this.data.twitter = dataa.twitter[0];
            }
            if (dataa.user_email && dataa.user_email[0]) {
              this.data.user_email = dataa.user_email;
            }
            localStorage.setItem(
              "dayleasing_user_type",
              JSON.stringify(data.json().posts)
            );
            this.auth.clearcookie();
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.logout();
      this.navCtrl.setRoot("LoginPage");
      this.auth.errtoast(err);
    }
  }
  loading: any;
  // Profile Pic
  lastImage: any = null;
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }
  imgname = "";
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      // quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      imagePath => {
        // Special handling for Android library
        if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          if (this.platform.is("ios")) {
            var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
            var correctPath = imagePath.substr(
              0,
              imagePath.lastIndexOf("/") + 1
            );
            this.imgname = currentName;
            this.lastImage = correctPath + "/" + currentName;
            this.uploadImage();
          } else if (this.platform.is("android")) {
            this.filePath.resolveNativePath(imagePath).then(filePath => {
              let correctPath = filePath.substr(
                0,
                filePath.lastIndexOf("/") + 1
              );
              let currentName = imagePath.substring(
                0,
                imagePath.lastIndexOf("?")
              );
              this.imgname = currentName;
              this.lastImage = currentName;
              this.uploadImage();
              // this.copyFileToLocalDir(
              //   correctPath,
              //   currentName,
              //   this.createFileName()
              // );
            });
          }
        } else {
          debugger;
          var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf("/") + 1);
          this.imgname = currentName;
          this.lastImage = correctPath + "/" + currentName;
          this.uploadImage();
          // this.copyFileToLocalDir(
          //   correctPath,
          //   currentName,
          //   this.createFileName()
          // );
        }
      },
      err => {
        // this.presentToast("Error while selecting image.");
        debugger;
      }
    );
  }

  // Create a new name for the image
  // private createFileName() {
  //   var d = new Date(),
  //     n = d.getTime(),
  //     newFileName = n + ".jpg";
  //   return newFileName;
  // }

  // Copy the image to a local folder
  // private copyFileToLocalDir(namePath, currentName, newFileName) {
  //   this.file
  //     .copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName)
  //     .then(
  //       success => {
  //         this.lastImage = newFileName;
  //       },
  //       error => {
  //         this.presentToast("Error while storing file.");
  //       }
  //     );
  // }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return "";
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = this.auth.updatepicapi();
    // var url = "https://beta.doveblasters.com/wp-json/wp/v2/media";

    // File for Upload
    // var targetPath = this.pathForImage(this.lastImage);
    var targetPath = this.lastImage;

    // File name only
    var filename = this.imgname;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: {
        fileName: filename,
        file: filename,
        user_id: this.auth.getuserId(),
        Authorization:
          "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token
      }
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: "Uploading..."
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(
      data => {
        this.loading.dismissAll();
        this.presentToast("Image succesfully uploaded.");
        this.getuserdata(JSON.parse(this.auth.getUser2()).username);
      },
      err => {
        this.loading.dismissAll();
        this.presentToast("Error while uploading file.");
      }
    );
  }

  updateprofile() {
    if (this.configs.valid == true) {
      this.loading = this.loadingCtrl.create({
        content: "Uploading..."
      });
      this.loading.present();
      this.auth.clearcookie();
      this.auth
        .updateprofileapi(this.configs.value, this.auth.getuserId())
        .subscribe(data => {
          this.loading.dismissAll();
          if (data) {
            if (data.json().status == 1) {
              this.auth.toast("Profile updated successfully");
              this.more();
              this.auth.clearcookie();
              this.getuserdata(JSON.parse(this.auth.getUser2()).username);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        });
    } else {
      this.auth.toast("Please fill all mandatory fields");
    }
  }

  // Profile Pic Id
  lastImageid: any = null;
  public presentActionSheetid() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePictureid(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.takePictureid(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }
  imgnameid = "";
  public takePictureid(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      imagePath => {
        // Special handling for Android library
        if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          if (this.platform.is("ios")) {
            var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
            var correctPath = imagePath.substr(
              0,
              imagePath.lastIndexOf("/") + 1
            );
            this.imgnameid = currentName;
            this.lastImageid = correctPath + "/" + currentName;
            this.uploadImageid();
          } else if (this.platform.is("android")) {
            this.filePath.resolveNativePath(imagePath).then(filePath => {
              let correctPath = filePath.substr(
                0,
                filePath.lastIndexOf("/") + 1
              );
              let currentName = imagePath.substring(
                imagePath.lastIndexOf("/") + 1,
                imagePath.lastIndexOf("?")
              );
              this.imgnameid = currentName;
              this.lastImageid = correctPath + currentName;
              this.uploadImageid();
              // this.copyFileToLocalDir(
              //   correctPath,
              //   currentName,
              //   this.createFileName()
              // );
            });
          }
        } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf("/") + 1);
          this.imgnameid = currentName;
          this.lastImageid = correctPath + "/" + currentName;
          this.uploadImageid();
          // this.copyFileToLocalDir(
          //   correctPath,
          //   currentName,
          //   this.createFileName()
          // );
        }
      },
      err => {
        // this.presentToast("Error while selecting image.");
      }
    );
  }

  // Create a new name for the image
  // private createFileNameid() {
  //   var d = new Date(),
  //     n = d.getTime(),
  //     newFileName = n + ".jpg";
  //   return newFileName;
  // }

  // Copy the image to a local folder
  // private copyFileToLocalDirid(namePath, currentName, newFileName) {
  //   this.file
  //     .copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName)
  //     .then(
  //       success => {
  //         this.lastImage = newFileName;
  //       },
  //       error => {
  //         this.presentToast("Error while storing file.");
  //       }
  //     );
  // }

  // Always get the accurate path to your apps folder
  public pathForImageid(img) {
    if (img === null) {
      return "";
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImageid() {
    // Destination URL
    var url = this.auth.updateidapi();
    // var url = "https://beta.doveblasters.com/wp-json/wp/v2/media";
    // File for Upload
    // var targetPath = this.pathForImage(this.lastImage);
    var targetPath = this.lastImageid;

    // File name only
    var filename = this.imgnameid;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: {
        fileName: filename,
        file: filename,
        user_id: this.auth.getuserId(),
        Authorization:
          "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token
      }
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: "Uploading..."
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(
      data => {
        this.loading.dismissAll();
        this.presentToast("Image succesfully uploaded.");
        this.getuserdata(JSON.parse(this.auth.getUser2()).username);
      },
      err => {
        this.loading.dismissAll();
        this.presentToast("Error while uploading file.");
      }
    );
  }

  updateprofileid() {
    if (this.configs.valid == true) {
      this.auth
        .updateprofileapi(this.configs.value, this.auth.getuserId())
        .subscribe(data => {});
    } else {
      this.auth.toast("Please fill all mandatory fields");
    }
  }
}
