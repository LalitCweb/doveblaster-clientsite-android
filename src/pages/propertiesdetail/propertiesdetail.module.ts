import {
  NgModule,
  NO_ERRORS_SCHEMA,
  CUSTOM_ELEMENTS_SCHEMA
} from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { PropertiesdetailPage } from "./propertiesdetail";
import { ComponentsModule } from "../../components/components.module";
import { AgmCoreModules } from "../../agm/core";
import { MyDatePickerModule } from "mydatepicker";
import { CountdownModule } from "ngx-countdown";
// import { CountdownModule } from "ng2-countdown-timer";

@NgModule({
  declarations: [PropertiesdetailPage],
  imports: [
    MyDatePickerModule,
    ComponentsModule,
    AgmCoreModules,
    // CountdownModule,
    CountdownModule,
    IonicPageModule.forChild(PropertiesdetailPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class PropertiesdetailPageModule {}
