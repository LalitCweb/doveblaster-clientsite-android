import {
  Component,
  trigger,
  state,
  style,
  transition,
  animate,
  group
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
  Platform
} from "ionic-angular";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";
import { errorHandler } from "@angular/platform-browser/src/browser";

/**
 * Generated class for the EditareainfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-editareainfo",
  templateUrl: "editareainfo.html"
})
export class EditareainfoPage {
  slots: any = [];
  today: any = new Date().toISOString();
  property: any;
  slot = {
    prop_area_name: "",
    prop_area_slot_details: "",
    prop_area_slot_color: ""
  };
  slot1 = {
    detail: [
      {
        start_date: "",
        end_date: "",
        price: ""
      }
    ]
  };
  max: any = "2099-10-31";
  myForm: FormGroup;
  myForm1: FormGroup;
  index: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    public auth: AuthProvider,
    public modalCtrl: ModalController,
    public platform: Platform,
    public viewCtrl: ViewController
  ) {
    this.myForm = this.fb.group({
      prop_area_name: ["", Validators.required],
      prop_area_slot_details: ["", Validators.required],
      prop_area_slot_color: ["", Validators.required]
    });
    this.myForm1 = this.fb.group({
      detail: this.fb.array([])
    });
    this.property = this.navParams.get("property_data").property;
    this.index = this.navParams.get("property_data").index;
    this.getsingledetailpage(this.property.property_id);
    // this.setSlot();
  }

  colorpick: boolean = false;
  selectedcolor = "#000";
  selectcolor(color) {
    this.colorpick = false;
    this.selectedcolor = color;
    this.myForm.controls.prop_area_slot_color.setValue(color);
  }
  selectcolorp() {
    this.colorpick = true;
  }
  getsingledetailpage(property_id) {
    this.auth.startloader();
    try {
      this.auth
        .getsinglealldetail(property_id, this.auth.getuserId())
        .subscribe(
          data => {
            if (data) {
              if (data.json().status == 1) {
                this.property = data.json().events;
                this.myForm1.reset();
                this.myForm.controls["prop_area_name"].setValue(
                  this.property.all_areas[this.index].area_name
                );
                this.myForm.controls["prop_area_slot_details"].setValue(
                  this.property.all_areas[this.index].slot_details
                );
                this.myForm.controls["prop_area_slot_color"].setValue(
                  this.property.all_areas[this.index].area_color
                );
                this.selectedcolor = this.property.all_areas[
                  this.index
                ].area_color;
                if (
                  this.property.all_areas[this.index].area_date_ranges &&
                  this.property.all_areas[this.index].area_date_ranges.length
                ) {
                  let control = <FormArray>this.myForm1.controls.detail;
                  if (control && control.length) {
                    for (var x = control.length; x >= 0; x--) {
                      control.removeAt(x);
                    }
                  }
                  for (
                    var slt = 0;
                    slt <
                    this.property.all_areas[this.index].area_date_ranges.length;
                    slt++
                  ) {
                    this.property.all_areas[this.index].area_date_ranges[slt];
                    control.push(
                      this.fb.group({
                        price: [
                          this.property.all_areas[this.index].area_date_ranges[
                            slt
                          ].price,
                          Validators.required
                        ],
                        start_date: [
                          this.property.all_areas[this.index].area_date_ranges[
                            slt
                          ].start_date,
                          Validators.required
                        ],
                        end_date: [
                          this.property.all_areas[this.index].area_date_ranges[
                            slt
                          ].end_date,
                          Validators.required
                        ]
                        // min: this.auth.checkdate(
                        //   control.value[control.value.length - 1].end_date,
                        //   1
                        // )
                      })
                    );
                  }
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
            this.auth.stoploader();
          },
          errorHandler => {
            this.auth.startloader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  ionViewDidLoad() {}

  chngedate(value) {
    if (value.value.start_date) {
    }
  }

  chngedatemin(value) {
    if (value.value.start_date) {
    }
  }

  addnewdate(page) {
    let profileModal = this.modalCtrl.create(page, {
      property_data: this.property,
      index: this.index
    });
    profileModal.present();
    profileModal.onDidDismiss(data => {
      if (data && data.property) {
        this.getsingledetailpage(data.property.property_id);
        this.index = data.index;
      }
    });
  }
  goback() {
    this.viewCtrl.dismiss();
  }
  setSlot() {
    let control = <FormArray>this.myForm1.controls.detail;
    if (control.valid == true) {
      if (control.value && control.value.length) {
        control.push(
          this.fb.group({
            price: ["", Validators.required],
            start_date: ["", Validators.required],
            end_date: ["", Validators.required],
            min: this.auth.checkdate(
              control.value[control.value.length - 1].end_date,
              1
            )
          })
        );
      } else {
        control.push(
          this.fb.group({
            price: ["", Validators.required],
            start_date: ["", Validators.required],
            end_date: ["", Validators.required],
            min: new Date().toISOString()
          })
        );
      }
    } else {
      this.auth.toast("Please fill all fields");
    }
  }

  deleteall() {
    let control = <FormArray>this.myForm.controls.detail;
    if (control && control.length) {
      for (var x = control.length; x >= 0; x--) {
        control.removeAt(x);
      }
    }
    control.push(
      this.fb.group({
        price: ["", Validators.required],
        start_date: ["", Validators.required],
        end_date: ["", Validators.required],
        min: new Date().toISOString()
      })
    );
  }
  deletedates(controls, date, index) {
    this.removerange(controls, date, index);
  }
  removerange(controls, date, index) {
    this.auth.startloader();
    try {
      this.auth
        .editstep33(
          this.auth.getuserId(),
          this.property.all_areas[this.index].area_date_ranges[index].id
        )
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              // this.slots.push(form);
              this.auth.toast(data.json().message);
              controls.removeAt(index);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        });
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  goto(page) {
    if (this.slots && this.slots.length) {
      this.property.slots = this.slots;
      this.navCtrl.push(page, {
        property_data: this.property
      });
    }
  }

  status: boolean = false;
  show: boolean = false;
  currentSelected: any = {};
  clickEvent(slot, index) {
    if (slot) {
      this.currentSelected = slot;
      this.currentSelected.index = index;
    }
    this.status = !this.status;
    this.show = !this.show;
  }

  addareas() {
    if (this.myForm.valid) {
      this.continue(this.myForm.value);
    } else {
      this.auth.toast("Please fill complete form");
    }
  }

  continue(form) {
    this.auth.startloader();
    try {
      this.auth
        .editstep31(
          this.auth.getuserId(),
          this.property.all_areas[this.index].area_id,
          form.prop_area_name,
          form.prop_area_slot_details,
          form.prop_area_slot_color
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                // this.slots.push(form);
                this.auth.toast(data.json().message);
                this.deleteall();
                this.myForm1.reset();
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  addareas2() {
    if (this.myForm1.valid) {
      var slt = [];
      slt.push(this.myForm1.value);
      this.continue1(slt, this.myForm1.value);
    } else {
      this.auth.toast("Please fill complete form");
    }
  }

  update() {}
  copy() {
    var slts = [];
    slts.push(this.currentSelected);
    this.continue1(slts, this.currentSelected);
    this.clickEvent(null, null);
  }
  delete() {
    this.slots.splice(this.currentSelected.index, 1);
    this.clickEvent(null, null);
  }

  continue1(slots, form) {
    this.addareas();
    // this.auth.startloader();
    // try {
    //   if (slots && slots.length) {
    //     this.auth
    //       .editstep32(
    //         this.auth.getuserId(),
    //         this.property.property_id,
    //         this.property.all_areas[this.index].area_id,
    //         firstdate,
    //         enddate,
    //         daterangeprice
    //       )
    //       .subscribe(data => {
    //         this.auth.stoploader();
    //         if (data) {
    //           if (data.json().status == 1) {
    //             // this.slots.push(form);
    //             this.deleteall();
    //             this.myForm1.reset();
    //           }
    //         }
    //       });
    //   }
    // } catch (err) {
    //   this.auth.stoploader();
    //   this.auth.errtoast(err);
    // }
  }
}
