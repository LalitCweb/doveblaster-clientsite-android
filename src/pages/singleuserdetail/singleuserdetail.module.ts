import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleuserdetailPage } from './singleuserdetail';

@NgModule({
  declarations: [
    SingleuserdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleuserdetailPage),
  ],
})
export class SingleuserdetailPageModule {}
