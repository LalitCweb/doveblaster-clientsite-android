import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the SingleuserdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-singleuserdetail",
  templateUrl: "singleuserdetail.html"
})
export class SingleuserdetailPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {
    console.log(this.navParams.get("authorid"));
    this.getdetail(this.navParams.get("authorid"));
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SingleuserdetailPage");
  }
  userdata: any = {};
  getdetail(cname) {
    this.auth.clearcookie();
    this.auth.startloader();
    try {
      this.auth.getuserdata(cname).subscribe(
        data => {
          this.auth.stoploader();
          if (data.json()) {
            if (data.json().status == 1) {
              console.log(data.json());
              this.auth.clearcookie();
              this.userdata = data.json().posts;
              this.userdata.user_name = cname;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.toast("Not able to get user data");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  viewactivity() {
    debugger;
    var user_email = "";
    if (this.userdata && this.userdata.user_email) {
      user_email = this.userdata.user_email;
    }
    var single_user_id = {
      user_email: user_email
    };
    this.navCtrl.push("MybookingdetailPage", {
      user_id: this.userdata.user_id,
      single_user_id: single_user_id
    });
  }
}
