import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { VerifyemailPage } from "./verifyemail";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [VerifyemailPage],
  imports: [ComponentsModule, IonicPageModule.forChild(VerifyemailPage)]
})
export class VerifyemailPageModule {}
