import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CancelledreservationPage } from "./cancelledreservation";
import { AgmCoreModules } from "../../agm/core";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [CancelledreservationPage],
  imports: [
    AgmCoreModules,
    PipesModule,
    IonicPageModule.forChild(CancelledreservationPage)
  ]
})
export class CancelledreservationPageModule {}
