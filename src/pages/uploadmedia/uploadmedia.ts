import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ActionSheetController,
  LoadingController,
  Platform,
  AlertController
} from "ionic-angular";
import { Camera } from "@ionic-native/camera";
import { FilePath } from "@ionic-native/file-path";
import { AuthProvider } from "../../providers/auth/auth";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";

/**
 * Generated class for the UploadmediaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-uploadmedia",
  templateUrl: "uploadmedia.html"
})
export class UploadmediaPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public platform: Platform,
    private filePath: FilePath,
    public auth: AuthProvider,
    public loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    public alertController: AlertController
  ) {
    if (this.navParams.get("property_data")) {
      this.getuserdetail();
    }
  }

  ionViewDidLoad() {}
  // Upload Image
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }

  loading: any;
  imgname = "";
  lastImage: any = null;
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      // quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      imagePath => {
        // Special handling for Android library
        if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          if (this.platform.is("ios")) {
            var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
            var correctPath = imagePath.substr(
              0,
              imagePath.lastIndexOf("/") + 1
            );
            this.imgname = currentName;
            this.lastImage = correctPath + "/" + currentName;
            this.uploadImage();
          } else if (this.platform.is("android")) {
            this.filePath.resolveNativePath(imagePath).then(filePath => {
              let correctPath = filePath.substr(
                0,
                filePath.lastIndexOf("/") + 1
              );
              let currentName = imagePath.substring(
                0,
                imagePath.lastIndexOf("?")
              );
              this.imgname = currentName;
              this.lastImage = currentName;
              this.uploadImage();
              // this.copyFileToLocalDir(
              //   correctPath,
              //   currentName,
              //   this.createFileName()
              // );
            });
          }
        } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf("/") + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf("/") + 1);
          this.imgname = currentName;
          this.lastImage = correctPath + "/" + currentName;
          this.uploadImage();
          // this.copyFileToLocalDir(
          //   correctPath,
          //   currentName,
          //   this.createFileName()
          // );
        }
      },
      err => {
        // this.auth.toast("Error while selecting image.");
      }
    );
  }

  public uploadImage() {
    this.auth.startloader();
    // Destination URL
    var url = this.auth.uploadmedia();

    // File for Upload
    // var targetPath = this.pathForImage(this.lastImage);
    var targetPath = this.lastImage;

    // File name only
    var filename = this.imgname;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: {
        fileName: filename,
        file: filename,
        user_id: this.auth.getuserId(),
        property_id: this.navParams.get("property_data").property_id,
        Authorization:
          "Bearer " + JSON.parse(localStorage.getItem("dayleasing_user")).token
      }
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: "Uploading..."
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(
      data => {
        this.auth.stoploader();
        this.loading.dismissAll();
        debugger;
        this.auth.toast("Image succesfully uploaded.");
        this.getuserdetail();
      },
      err => {
        this.auth.stoploader();
        this.loading.dismissAll();
        debugger;
        this.auth.toast("Error while uploading file.");
      }
    );
  }

  video_id: any = "";
  video_type: any = "";
  savevideo() {
    if (this.video_id != "" && this.video_type != "") {
      this.auth.startloader();
      try {
        this.auth
          .savevideo(
            this.auth.getuserId(),
            this.navParams.get("property_data").property_id,
            this.video_type,
            this.video_id
          )
          .subscribe(
            data => {
              this.auth.stoploader();
              if (data) {
                if (data.json() && data.json().status == 1) {
                  this.auth.toast(data.json().message);
                  this.getuserdetail();
                } else if (data.json().status == 2) {
                  this.auth.logout();
                  this.navCtrl.setRoot("LoginPage");
                  this.auth.toast("Session expired. Please login again.");
                } else if (data.json() && data.json().status == 0) {
                  if (data.json().message) {
                    this.auth.toast(data.json().message);
                  } else {
                    this.auth.toast("Currently not able to save data");
                  }
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else {
      this.auth.toast("Fill video details and choose type");
    }
  }
  mymedia: any;
  getuserdetail() {
    this.auth.startloader();
    try {
      this.auth
        .getsinglealldetail(
          this.navParams.get("property_data").property_id,
          this.auth.getuserId()
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data.json()) {
              if (data.json().status == 1) {
                this.mymedia = data.json().events;
                if (this.mymedia.embed_video_id) {
                  this.video_id = this.mymedia.embed_video_id;
                  this.video_type = this.mymedia.embed_video_type;
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  async presentAlertConfirm(media) {
    const alert = await this.alertController.create({
      message: "Are you sure you want delete this media",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.deletemedia(media);
          }
        }
      ]
    });

    await alert.present();
  }
  deletemedia(media) {
    try {
      this.auth
        .deletemedia(
          this.auth.getuserId(),
          media.image_id,
          this.navParams.get("property_data").property_id
        )
        .subscribe(
          data => {
            if (data.json()) {
              if (data.json().status == 1) {
                this.auth.toast(data.json().message);
                this.getuserdetail();
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              } else {
                if (data.json().message) {
                  this.auth.toast(data.json().message);
                }
              }
            }
          },
          errorHandler => {}
        );
    } catch (err) {
      this.auth.errtoast(err);
    }
  }
  goto() {
    this.navCtrl.setRoot("PropertieslistviewPage");
  }
}
