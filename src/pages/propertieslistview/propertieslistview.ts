import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate,
  group,
  ViewChild,
  ElementRef,
  NgZone
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  MenuController,
  Platform,
  ModalController,
  ActionSheetController,
  Content,
  AlertController,
  Slides
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { AuthProvider } from "../../providers/auth/auth";
import { TracklocationProvider } from "../../providers/tracklocation/tracklocation";
import { GoogleMapsProvider } from "../../providers/google-maps/google-maps";
import { FcmProvider } from "../../providers/fcm/fcm";
import { IMyDpOptions } from "mydatepicker";
import { DomSanitizer } from "@angular/platform-browser";
import "rxjs/add/operator/first";
import { DatePipe } from "@angular/common";
import firebase from "firebase";
import { AngularFirestore, QuerySnapshot } from "angularfire2/firestore";
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { AgmMap } from "@agm/core";
import { NetworkInterface } from "@ionic-native/network-interface";
declare var google: any;
declare var window: any;
declare var Keyboard: any;
declare var $: any;

@IonicPage()
@Component({
  selector: "page-propertieslistview",
  animations: [
    trigger("slideInOut", [
      state("in", style({ height: "*", opacity: 0 })),
      transition(":leave", [
        style({ height: "*", opacity: 1 }),

        group([
          animate(300, style({ height: 0 })),
          animate("300ms ease-in-out", style({ opacity: "0" }))
        ])
      ]),
      transition(":enter", [
        style({ height: "0", opacity: 0 }),

        group([
          animate(300, style({ height: "*" })),
          animate("300ms ease-in-out", style({ opacity: "1" }))
        ])
      ])
    ])
  ],
  templateUrl: "propertieslistview.html"
})
export class PropertieslistviewPage implements OnInit {
  @ViewChild(Slides) slides: Slides;
  myDatePickerOptions: IMyDpOptions = {
    dateFormat: "mm-dd-yyyy"
  };
  service: any;
  placesService: any;
  @ViewChild("AgmMap") public agmMap: AgmMap;
  @ViewChild("map_canvas") mapElements: ElementRef;
  @ViewChild(Content)
  content: Content;
  maxDate: any = "2999-10-31";
  view: any;
  showloader: any = true;
  listView: any = true;
  mapView: any = false;
  dateView: any = false;
  marker = "assets/images/stillmarker.gif";
  order: string;
  reverse: boolean = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public menuCtrl: MenuController,
    public platform: Platform,
    private geolocation: Geolocation,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public auth: AuthProvider,
    private zone: NgZone,
    public maps: GoogleMapsProvider,
    public track: TracklocationProvider,
    public fcmp: FcmProvider,
    public alertCtrl: AlertController,
    public datepipe: DatePipe,
    private sanitizer: DomSanitizer,
    private db: AngularFireDatabase,
    private networkInterface: NetworkInterface
  ) {
    this.platform.ready().then(() => {
      this.service = new google.maps.places.AutocompleteService();
      this.getlease(true);
    });
    var dt: any = new Date();
    this.startdate = {
      date: {
        day: dt.getDate(),
        month: dt.getMonth() + 1,
        year: dt.getFullYear()
      }
    };
    this.enddate = {
      date: {
        day: dt.getDate(),
        month: dt.getMonth() + 1,
        year: dt.getFullYear()
      }
    };

    this.auth.clearcookie();
    this.view = "list";
  }
  /******************/
  /***Touch Events***/
  /******************/
  touchstart(e) {
    if (e.cancelable) {
      // e.preventDefault();
    }
  }
  //*********************************//
  //*********Get advertisement*******//
  //*********************************//
  advertisement: any = [];
  getadvertisement() {
    try {
      this.auth.getadvertisements().subscribe(
        data => {
          if (data.json() && data.json().status == 1) {
            var myadv: any;
            myadv = data.json().data;
            this.advertisement = [];
            // this.advertisement = data.json().data;
            myadv.forEach((item, index) => {
              var cc: any = this;
              if (item.data.ad_type == "active") {
                cc.advertisement.push(item);
              }
            });
            this.advertisement.forEach((item, index) => {
              var cc: any = this;
              if (cc.platform.is("ios")) {
                cc.advertisement[
                  index
                ].ad_bannercode = item.ad_bannercode.replace("_blank", "_self");
              }
              cc.advertisement[index].ad_bannercode = JSON.stringify(
                item.ad_bannercode
              ).replace("%asset%", item.data.ad_image);

              cc.advertisement[index].ad_bannercode = JSON.parse(
                cc.advertisement[index].ad_bannercode
              ).replace(/\\/g, "");
            });
            this.auth.saveads(this.advertisement);
          }
        },
        errorHandler => {}
      );
    } catch (err) {}
  }
  /******************/
  /***Change lease***/
  /******************/
  changelease1(value) {
    if (value) {
    } else {
      this.action_values = [];
      for (var x = 0; x < this.lease.length; x++) {
        if (this.lease[x].search) {
          this.action_values.push(this.lease[x].slug);
        } else {
          this.action_values.splice(x, 1);
        }
      }
    }
  }
  changelease(value) {
    for (var x = 0; x < this.lease.length; x++) {
      if (value == true) {
        if (this.lease[x].search) {
          if (x == this.lease.length - 1) {
            this.auth.clearcookie();
            this.getallproperty();
            this.getallpropertyformap();
            if (this.auth.isOwner() == false) {
              setTimeout(() => {
                this.myreservations();
              }, 1000);
            }
          }
        } else {
          this.lease[x].search = true;
          this.action_values.push(this.lease[x].slug);
          if (x == this.lease.length - 1) {
            this.auth.clearcookie();
            this.getallproperty();
            this.getallpropertyformap();
            if (this.auth.isOwner() == false) {
              setTimeout(() => {
                this.myreservations();
              }, 1000);
            }
          }
        }
      } else {
      }
    }
  }
  /******************/
  /*****Sort by******/
  /******************/
  setOrder(value: string) {
    if (value == "htol") {
      this.reverse = true;
      this.order = "property_min_price";
    } else if (value == "ltoh") {
      this.reverse = false;
      this.order = "property_min_price";
    } else {
      this.order = null;
      this.reverse = true;
    }
  }

  ngOnInit() {
    this.auth.clearcookie();
    this.findlocation();
  }
  ngAfterViewInit() {
    this.auth.clearcookie();
    this.content.ionScrollEnd.subscribe(data => {});
    var tis = this;
    setTimeout(() => {
      tis.agmMap.triggerResize();
    }, 900);
  }
  setsliders() {
    if (this.slides) {
      setTimeout(() => {
        this.setslider();
      }, 2000);
    }
  }
  setslider() {
    this.slides.startAutoplay();
  }
  taketotop: any = false;
  onScroll(data) {
    if (data.scrollTop > 60 && this.mapView == true) {
      this.taketotop = true;
    } else {
      this.taketotop = false;
    }
  }
  ionViewWillEnter() {
    if (this.platform.is("android")) {
      this.listView = true;
      this.mapView = false;
      this.dateView = false;
    }
    if (this.auth.getProperties()) {
      this.properties = this.auth.getProperties();
    }
    this.autocompleteItems = [];
    this.menuCtrl.enable(true);
    this.auth.clearcookie();
    // if (this.auth.havingads()) {
    //   this.advertisement = [];
    //   this.advertisement = null;
    //   this.advertisement = this.auth.getads();
    // } else {
    this.getadvertisement();
    // }
    this.setsliders();
    this.pleaseclearwatch();
    this.platform.ready().then(() => {
      this.networkInterface
        .getWiFiIPAddress()
        .then(address => {
          // console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`);
          this.myip = address.ip;
        })
        .catch(error => console.error(`Unable to get IP: ${error}`));
      this.networkInterface
        .getCarrierIPAddress()
        .then(address => {
          // console.info(`IP: ${address.ip}, Subnet: ${address.subnet}`);
          this.myip = address.ip;
        })
        .catch(error => console.error(`Unable to get IP: ${error}`));
    });
  }
  ionViewDidLoad() {
    this.auth.clearcookie();
    var tis = this;
  }
  ionViewDidEnter() {}
  adupdate: any = [];
  slideChanged() {
    this.slides.startAutoplay();
    let currentIndex = this.slides.getActiveIndex();
    if (currentIndex > this.advertisement.length) {
      currentIndex = 1;
    }
    if (this.advertisement[currentIndex - 1].count == undefined) {
      this.advertisement[currentIndex - 1].count = 1;
    } else if (
      this.advertisement[currentIndex - 1] &&
      this.advertisement[currentIndex - 1].count > 0
    ) {
      this.advertisement[currentIndex - 1].count++;
    }
  }
  ionViewWillLeave() {
    this.auth.clearcookie();
    this.slides.stopAutoplay();
    for (var x = 0; x < this.advertisement.length; x++) {
      this.updateads(this.advertisement[x]);
    }
  }
  doRefresh(event) {
    setTimeout(() => {
      event.complete();
    }, 2000);
    this.properties = [];
    this.listV();
  }
  offset: any = -1;
  properties: any = [];
  getallproperty() {
    this.auth.clearcookie();
    try {
      this.offset = this.offset + 2;
      this.auth.getallproperties(this.auth.getuserId(), this.offset).subscribe(
        data => {
          if (data) {
            if (data && data.json() && data.json().status == 1) {
              var dataa: any = data.json().events;
              this.properties = [];
              for (var x = 0; x < dataa.length; x++) {
                if (dataa[x].property_min_price) {
                  dataa[x].property_min_price = parseInt(
                    dataa[x].property_min_price
                  );
                } else {
                  dataa[x].property_min_price = 0;
                }
                this.properties.push(dataa[x]);
                this.auth.saveProperties(this.properties);
              }
            } else if (data.json().status == 2) {
              this.auth.toast("Session expired. Please login again.");
            }
          }
        },
        errorHandler => {
          // this.auth.stoploader();
          // this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
          this.auth.setautorelogin(this.relogintry);
          this.relogintry++;
          if (this.relogintry > 5) {
            this.confirmAlertpropertiesl();
          }
          this.gologin(
            this.auth.getlogincredentials().username,
            this.auth.getlogincredentials().password
          );
        }
      );
    } catch (err) {
      // this.auth.stoploader();
      // this.auth.errtoast(err);
      this.auth.setautorelogin(this.relogintry);
      this.relogintry++;
      if (this.relogintry > 5) {
        this.confirmAlertpropertiesl();
      }
    }
  }
  relogintry: any = 0;
  /////////////////////
  //////Relogin////////
  /////////////////////

  rememberme: any = true;
  gologin(email, password) {
    this.auth.clearcookie();
    this.auth.startloader();
    this.auth.postlogin(email, password).subscribe(
      data => {
        var user = data.json();
        if (user.token != undefined) {
          localStorage.setItem("dayleasing_user", JSON.stringify(user));
          this.auth.clearcookie();
          this.getdetail(email);
          this.auth.stoploader();
          if (this.rememberme) {
            this.auth.savelogincredentials({
              username: email,
              password: password,
              rememberme: "true"
            });
          } else {
            // this.auth.removelogincredentials();
            this.auth.savelogincredentials({
              username: email,
              password: password,
              rememberme: "false"
            });
          }
        } else if (user.data && user.data.status && user.data.status == 403) {
          if (user.message) {
            this.auth.toast(user.message);
          } else {
            this.auth.toast("Something problem with login ");
          }
          this.auth.stoploader();
        }
        // localStorage.setItem("dayleasing_user", "");
      },
      errorHandler => {
        if (
          errorHandler.json().data &&
          errorHandler.json().data.status &&
          errorHandler.json().data.status == 403
        ) {
          if (errorHandler.json().code == "[jwt_auth] invalid_username") {
            this.auth.toast("Invalid username.");
          } else if (
            errorHandler.json().code == "[jwt_auth] incorrect_password"
          ) {
            this.auth.toast("The password you entered is incorrect..");
          } else {
            this.auth.toast("Something problem with login ");
          }
          this.auth.stoploader();
        }
      },
      () => {
        this.auth.stoploader();
      }
    );
  }

  getdetail(cname) {
    this.auth.clearcookie();
    try {
      this.auth.getuserdata(cname).subscribe(
        data => {
          if (data.json()) {
            if (data.json().status == 1) {
              this.auth.clearcookie();
              localStorage.setItem(
                "dayleasing_user_type",
                JSON.stringify(data.json().posts)
              );
              this.auth.clearcookie();
              this.generatefcm();
              this.auth.clearcookie();
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.toast("Not able to get user data");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  generatefcm() {
    this.auth.clearcookie();
    this.navCtrl.setRoot("PropertieslistviewPage");
  }

  productdetail: any;
  getpropertydetail(marker) {
    this.auth.startloader();
    try {
      this.auth.getsinglealldetail(marker, this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data && data.json() && data.json().status == 1) {
              this.productdetail = data.json().events;
              this.productdetail.ID = marker;
              this.status = true;
              this.show = true;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          // this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      // this.auth.toast(e);
    }
  }

  propertiesmap: any = [];
  getallpropertyformap() {
    this.auth.clearcookie();
    this.auth.startloader();
    try {
      this.auth.getallproperty(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data && data.json() && data.json().status == 1) {
            this.propertiesmap = data.json().all_data;
            if (this.propertiesmap && this.propertiesmap.length) {
              var infowindow = new google.maps.InfoWindow();
              for (var prop = 0; prop < this.propertiesmap.length; prop++) {
                var el = this;
                var myLatlng: any = new google.maps.LatLng(
                  el.auth.convertToint(
                    el.propertiesmap[prop].newdata[0].meta_value
                  ),
                  el.auth.convertToint(
                    el.propertiesmap[prop].newdata[1].meta_value
                  )
                );
                var marker: any = new google.maps.Marker({
                  position: myLatlng,
                  icon: el.propertiesmap[prop].pin_url,
                  title: el.propertiesmap[prop].newdata[0].post_id
                });
                // To add the marker to the map, call setMap();
                marker.setMap(this.map_canvas);
                google.maps.event.addListener(
                  marker,
                  "click",
                  (function(marker, prop) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    return () => {
                      el.call(el.propertiesmap[prop].newdata[0].post_id);
                    };
                  })(marker, prop)
                );
              }
            }
          } else if (data.json().status == 2) {
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
            this.auth.toast("Session expired. Please login again.");
          } else if (data.json().status == 0) {
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
            this.propertiesmap = [];
            // this.auth.toast(data.json().message);
          } else {
            this.auth.toast("Not getting properties for map");
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.confirmAlertproperties();
          // this.auth.errtoast(errorHandler);
          // this.auth.logout();
          // this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.confirmAlertproperties();
      // this.auth.errtoast(err);
    }
  }
  call(value) {
    this.clickEvent(value);
  }

  presentSearchModal() {
    let searchModal: any;
    if (this.category_values || this.all_checkers) {
      var dt = {
        category_values: this.category_values,
        all_checkers: this.all_checkers,
        price: { lower: this.price_low, upper: this.price_max }
      };
      searchModal = this.modalCtrl.create("SearchPage", { checked_data: dt });
      searchModal.present();
    } else {
      searchModal = this.modalCtrl.create("SearchPage");
      searchModal.present();
    }
    searchModal.onDidDismiss(data => {
      if (data) {
        if (
          (data && data.category_values) ||
          data.all_checkers ||
          data.price.lower != 1 ||
          data.price.upper != 5000
        ) {
          this.category_values = data.category_values;
          this.all_checkers = data.all_checkers;
          this.price_low = data.price.lower;
          this.price_max = data.price.upper;
          if (this.listView) {
            this.getsearchdata();
          } else if (this.mapView) {
            this.getsearchdatamap();
          }
        }
      }
    });
  }
  setdatesrch() {
    this.listView = true;
    this.mapView = false;
    this.dateView = false;

    if (
      this.auth.changeformat(this.startdate) &&
      this.auth.changeformat(this.enddate)
    ) {
      this.getsearchdata();
    } else {
      this.getsearchdata();
    }
  }
  status: boolean = false;
  show: boolean = false;
  clickEvent(marker) {
    if (marker) {
      this.getpropertydetail(marker);
    } else {
      this.status = !this.status;
      this.show = !this.show;
    }
  }
  //**********************//
  //*********Filter*******//
  //**********************//
  startdate: any = {
    date: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    }
  };
  enddate: any = {
    date: {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    }
  };
  category_values: any;
  action_values: any = [];
  all_checkers: any;
  property_admin_area: any = "";
  country: any;
  price_low: any = 1;
  price_max: any = 5000;
  selectionshow: boolean = false;
  city: any;
  propertiesmapfilter() {
    if (this.selectionshow) {
      this.selectionshow = false;
      if (this.listView) {
        this.getsearchdata();
      } else if (this.mapView) {
        this.getsearchdatamap();
      }
    } else if (this.selectionshow == false) {
      this.selectionshow = true;
    }
  }
  chooseItem(value, el) {
    this.property_admin_area = "";
    this.city = "";
    el.placesService.getDetails(
      {
        placeId: value.place_id
      },
      (details: any) => {
        this.zone.run(() => {
          this.searchinput = details.name;
          for (var srch = 0; srch < details.address_components.length; srch++) {
            if (details.address_components[srch].types[0] == "locality") {
              this.searchinput = details.address_components[srch].long_name;
              this.city = details.address_components[srch].long_name;
            } else if (details.address_components[srch].types[0] == "country") {
              this.country = details.address_components[srch].long_name.replace(
                /-/g,
                " "
              );
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_2"
            ) {
              this.property_admin_area = this.wpestate_build_admin_area(
                details.address_components[srch].long_name
              );
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_1"
            ) {
              this.property_admin_area = this.wpestate_build_admin_area(
                details.address_components[srch].long_name
              );
            }
            if (srch == details.address_components.length - 1) {
              this.property_admin_area = this.property_admin_area.replace(
                /^,/,
                ""
              );
              this.property_admin_area = this.property_admin_area
                .replace(/\s+/g, " ")
                .trim();
              if (
                this.all_checkers == null ||
                this.all_checkers == undefined ||
                this.all_checkers.length == 0
              ) {
                this.all_checkers = [];
              }
              if (this.listView) {
                this.getsearchdata();
              } else if (this.mapView) {
                this.getsearchdatamap();
              }
            }
          }
          this.autocompleteItems = [];
        });
      }
    );
  }
  selecteddate: boolean = false;
  datechange(date) {
    this.selecteddate = true;
    setTimeout(() => {
      if (Keyboard.isVisible) {
        Keyboard.hide();
      }
    }, 1000);
  }
  wpestate_build_admin_area(admin_area) {
    if (this.property_admin_area === "") {
      this.property_admin_area = admin_area;
    } else {
      this.property_admin_area = this.property_admin_area + ", " + admin_area;
    }
    return this.property_admin_area;
  }

  autocompleteItems;
  searchinput: any;
  searchdata() {
    if (this.searchinput == "") {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions(
      {
        input: this.searchinput
      },
      (predictions, status) => {
        me.autocompleteItems = [];

        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach(prediction => {
              me.autocompleteItems.push(prediction);
            });
          }
        });
      }
    );
    var value = {
      city: this.searchinput
    };
  }

  lease: any = [];
  getlease(value) {
    this.auth.clearcookie();
    this.auth.startloader();
    try {
      this.auth.getlease(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().all_action_categories) {
              this.lease = data.json().all_action_categories;
              if (value) {
                this.auth.clearcookie();
                this.changelease(true);
              }
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
        }
      );
    } catch (err) {
      this.auth.stoploader();
    }
  }

  clearsearch() {
    var dt: any = new Date();
    this.startdate = {
      date: {
        year: dt.getFullYear(),
        month: dt.getMonth() + 1,
        day: dt.getDate()
      }
    };
    this.enddate = {
      date: {
        year: dt.getFullYear(),
        month: dt.getMonth() + 1,
        day: dt.getDate()
      }
    };
    this.category_values = "";
    this.all_checkers = "";
    this.searchinput = "";
    this.city = "";
    this.property_admin_area = "";
    this.country = "";
    this.price_low = 1;
    this.price_max = 5000;
    this.properties = [];
    this.changelease(true);
  }

  getsearchdatamap() {
    this.auth.startloader();
    this.auth.clearcookie();
    if (
      this.category_values == "" ||
      this.category_values == null ||
      this.category_values == undefined
    ) {
      this.category_values = "all";
    }
    if (
      this.action_values == "" ||
      this.action_values == null ||
      this.action_values == undefined
    ) {
      this.category_values = [];
    }
    try {
      this.auth
        .serchformap(
          this.selecteddate,
          this.auth.changeformat(this.startdate),
          this.auth.changeformat(this.enddate),
          this.category_values,
          this.action_values,
          this.all_checkers,
          this.city,
          this.property_admin_area,
          this.country,
          this.price_low,
          this.price_max
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            this.propertiesmap = [];
            if (data && data.json() && data.json().status == 1) {
              this.auth.stoploader();
              this.propertiesmap = data.json().all_data;
              if (this.propertiesmap && this.propertiesmap.length) {
                var infowindow = new google.maps.InfoWindow();
                for (var prop = 0; prop < this.propertiesmap.length; prop++) {
                  var myLatlng = new google.maps.LatLng(
                    this.auth.convertToint(
                      this.propertiesmap[prop].newdata[0].meta_value
                    ),
                    this.auth.convertToint(
                      this.propertiesmap[prop].newdata[1].meta_value
                    )
                  );
                  var marker = new google.maps.Marker({
                    position: myLatlng,
                    icon: this.propertiesmap[prop].pin_url,
                    title: this.propertiesmap[prop].newdata[0].post_id
                  });
                  var el = this;
                  // To add the marker to the map, call setMap();
                  marker.setMap(this.map_canvas);
                  google.maps.event.addListener(
                    marker,
                    "click",
                    (function(marker, prop) {
                      //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                      return function() {
                        el.call(el.propertiesmap[prop].newdata[0].post_id);
                      };
                    })(marker, prop)
                  );
                }
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json().status == 0) {
              // this.auth.logout();
              // this.navCtrl.setRoot("LoginPage");
              this.auth.toast(data.json().message);
            } else {
              this.auth.toast("Not getting properties for map");
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (e) {
      this.auth.stoploader();
      // this.auth.toast(e);
    }
  }
  getsearchdata() {
    if (
      this.category_values == "" ||
      this.category_values == null ||
      this.category_values == undefined
    ) {
      this.category_values = "all";
    }
    if (
      this.action_values == "" ||
      this.action_values == null ||
      this.action_values == undefined
    ) {
      this.category_values = [];
    }
    this.auth.startloader();
    this.auth.clearcookie();
    try {
      this.auth
        .searchdata(
          this.selecteddate,
          this.auth.changeformat(this.startdate),
          this.auth.changeformat(this.enddate),
          this.category_values,
          this.action_values,
          this.all_checkers,
          this.city,
          this.property_admin_area,
          this.country,
          this.price_low,
          this.price_max
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              this.properties = [];
              if (data && data.json() && data.json().status == 1) {
                if (data.json().events) {
                  for (var x = 0; x < data.json().events.length; x++) {
                    if (data.json().events[x].property_min_price) {
                      data.json().events[x].property_min_price = parseInt(
                        data.json().events[x].property_min_price
                      );
                    } else {
                      data.json().events[x].property_min_price = 0;
                    }
                  }

                  this.properties = data.json().events;
                } else {
                  this.auth.toast("No Property");
                }
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json().status == 0) {
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
          }
        );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }

  mapV() {
    if (this.mapView == false) {
      this.mapView = true;
      this.listView = false;
      this.dateView = false;
      if (
        this.category_values ||
        (this.all_checkers && this.all_checkers.length) ||
        this.searchinput ||
        this.property_admin_area ||
        this.country ||
        this.price_low != 1 ||
        this.price_max != 5000 ||
        this.action_values.length == 1
      ) {
        this.getsearchdatamap();
      } else {
        this.getallpropertyformap();
      }
      this.findlocation();
    } else {
      this.auth.startloader();
      this.platform.ready().then(() => {
        this.geolocation
          .getCurrentPosition()
          .then(resp => {
            this.auth.stoploader();
            this.options.lat = this.auth.convertToint(resp.coords.latitude);
            this.options.lng = this.auth.convertToint(resp.coords.longitude);
            if (!this.mapView) {
              this.options.zoom = 10;
            }
            this.agm_map.setCenter({
              lat: resp.coords.latitude,
              lng: resp.coords.longitude
            });
          })
          .catch(error => {
            this.auth.stoploader();
          });
      });
    }
  }

  changelocation(moveto, current) {
    var deltalat = (moveto.lat - current.lat) / 100;
    var deltalng = (moveto.lng - current.lng) / 100;

    var delay = 10 * 0.5;
    if (deltalat > 0 && deltalng > 0) {
      for (var i = 0; i < 100; i++) {
        (function(ind) {
          setTimeout(function() {
            var lat = moveto.lat;
            var lng = moveto.lng;
            lat += deltalat;
            lng += deltalng;
            this.options.lat = lat;
            this.options.lng = lng;
          }, delay * ind);
        })(i);
      }
    }
  }
  agm_map: any;
  findlocation() {
    this.platform.ready().then(() => {
      this.geolocation
        .getCurrentPosition()
        .then(resp => {
          this.options.lat = resp.coords.latitude;
          this.options.lng = resp.coords.longitude;
          this.initializeMap();
        })
        .catch(error => {});
    });
  }
  listV() {
    this.listView = true;
    this.mapView = false;
    this.dateView = false;
    if (
      this.category_values ||
      (this.all_checkers && this.all_checkers.length) ||
      this.searchinput ||
      this.property_admin_area ||
      this.country ||
      this.price_low != 1 ||
      this.price_max != 5000 ||
      this.action_values.length == 1
    ) {
      this.getsearchdata();
    } else {
      this.getallproperty();
    }
  }
  dateV() {
    if (this.dateView == false) {
      this.dateView = true;
      this.mapView = false;
      this.listView = false;
    } else {
      this.listView = true;
      this.mapView = false;
      this.dateView = false;
    }
  }

  goto(page, data) {
    this.navCtrl.push(page, {
      property_data: data
    });
  }

  mapdetail(productdetail) {
    this.status = !this.status;
    this.show = !this.show;
    this.navCtrl.push("PropertiesdetailPage", {
      property_data: productdetail,
      property_detail: true
    });
  }
  // Google map
  map_canvas: any;
  initializeMap = (): void => {
    this.platform.ready().then(() => {
      let mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(this.options.lat, this.options.lng),
        tilt: 0,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        },
        mapTypeId: "hybrid",
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER
        },
        mapTypeControl: true,
        fullscreenControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        }
      };

      this.map_canvas = new google.maps.Map(
        this.mapElements.nativeElement,
        mapOptions
      );

      this.placesService = new google.maps.places.PlacesService(
        this.map_canvas
      );
    });
  };

  // Agm Map

  polygon: any;
  circle: any;
  rectangle: any;
  options: any = {
    lat: 31.9686,
    lng: 99.9018,
    zoom: 2,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    radius: 29.232835208047494,
    panControl: true,
    rotateControl: true,
    scaleControl: true,
    mapTypeControl: true
  };

  managerOptions = {
    drawingControl: true,
    drawingControlOptions: {
      drawingModes: ["polygon", "rectangle", "circle"]
    },
    polygonOptions: {
      draggable: true,
      editable: true
    },
    drawingMode: "polygon"
  };
  private _map: any;
  changeTilt(map) {
    this._map = map;
    this._map.setTilt(0);
    this.agm_map = map;
    map.setOptions({
      zoomControl: "false",
      mapTypeControl: true,
      mapTypeId: "hybrid",
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_CENTER
      },
      streetViewControl: "true",
      streetViewControlOptions: {
        position: google.maps.ControlPosition.TOP_LEFT
      }
    });
  }
  circleCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = null;
    this.rectangle = null;
    this.circle = $event;
  }

  rectangleCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = null;
    this.circle = null;
    this.rectangle = $event;
  }

  polygonCreated($event) {
    if (this.polygon) {
      this.polygon.setMap(null);
    }
    if (this.rectangle) {
      this.rectangle.setMap(null);
    }
    if (this.circle) {
      this.circle.setMap(null);
    }
    this.polygon = $event;
    this.addPolygonChangeEvent(this.polygon);
    google.maps.event.addListener(this.polygon, "coordinates_changed", function(
      index,
      obj
    ) {
      // Polygon object: yourPolygon
    });
  }

  getPaths() {
    if (this.polygon) {
      const vertices = this.polygon.getPaths().getArray()[0];
      let paths = [];
      vertices.getArray().forEach(function(xy, i) {
        let latLng = {
          lat: xy.lat(),
          lng: xy.lng()
        };
        paths.push(JSON.stringify(latLng));
      });
      return paths;
    }
    return [];
  }

  addPolygonChangeEvent(polygon) {
    var me = polygon,
      isBeingDragged = false,
      triggerCoordinatesChanged = function() {
        // Broadcast normalized event
        google.maps.event.trigger(me, "coordinates_changed");
      };

    // If  the overlay is being dragged, set_at gets called repeatedly,
    // so either we can debounce that or igore while dragging,
    // ignoring is more efficient
    google.maps.event.addListener(me, "dragstart", function() {
      isBeingDragged = true;
    });

    // If the overlay is dragged
    google.maps.event.addListener(me, "dragend", function() {
      triggerCoordinatesChanged();
      isBeingDragged = false;
    });

    // Or vertices are added to any of the possible paths, or deleted
    var paths = me.getPaths();
    paths.forEach(function(path, i) {
      google.maps.event.addListener(path, "insert_at", function() {
        triggerCoordinatesChanged();
      });
      google.maps.event.addListener(path, "set_at", function() {
        if (!isBeingDragged) {
          triggerCoordinatesChanged();
        }
      });
      google.maps.event.addListener(path, "remove_at", function() {
        triggerCoordinatesChanged();
      });
    });
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      infiniteScroll.complete();
      this.auth.clearcookie();
      this.getallproperty();
      this.myreservations();
    }, 500);
  }

  pageScroller() {
    //scroll to page top
    this.content.scrollToTop();
  }

  //////////////////////////
  //////Notification////////
  //////////////////////////

  sendnotification: any = [];
  getmessage(property_id, index) {
    this.sendnotification = [];
    try {
      this.auth
        .getmessages1(
          this.auth.convertToint(property_id),
          this.auth.getuserId()
        )
        .subscribe(
          data => {
            if (data.json() && data.json().status == 1) {
              // this.sendnotification[
              //   index
              // ].upon_arival = data.json().prop_pn_upon_arival;
              // this.sendnotification[index].now = data.json().prop_pn_now;
              this.sendnotification.push({
                upon_arival: data.json().prop_pn_upon_arival,
                now: data.json().prop_pn_now
              });
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  myip: any;
  updateads(data) {
    var ddt: any = {
      ad_id: data.data.ad_id,
      group_id: data.data.lm_group,
      remote_ip: this.myip,
      counting: data.count
    };
    try {
      this.auth.updateads(ddt).subscribe(
        data => {
          if (data.json() && data.json().status == 1) {
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  updateadclick(data) {
    var ddt: any = {
      ad_id: data.data.ad_id,
      group_id: data.data.lm_group,
      remote_ip: this.myip,
      counting: data.count
    };
    try {
      this.auth.updateadclick(ddt).subscribe(
        data => {
          if (data.json() && data.json().status == 1) {
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }

  logintoken: any;
  logintokenuser: any;
  logintokenSubscribe: any;
  logintokenuserSubscribe: any;
  calculatedistances: any;
  watch: any;
  notify: any;
  getlocation() {
    var el: any = this;
    if (localStorage.getItem("mydayleasingtoken")) {
    } else {
      // el.track.generatefcmtoken();
    }
    el.track.getlocation();
    // debugger;
    // this.watchingposition = el.geolocation.watchPosition();
    this.watchingposition = setInterval(() => {
      // debugger;
      for (var res = 0; res < el.selectedreservation.length; res++) {
        if (el.auth.isLogin()) {
          if (
            (el.selectedreservation[res].property_details[0].prop_total_area ==
              "" ||
              el.selectedreservation[res].property_details[0].prop_total_area ==
                null,
            el.selectedreservation[res].property_details[0].prop_total_area ==
              undefined)
          ) {
          } else {
            //////////////////////////////////////
            ////////Property Outline /////////////
            /////////////////////////////////////
            if (
              el.selectedreservation[res].property_details[0]
                .prop_area_shape_type == "polygon"
            ) {
              if (
                typeof el.selectedreservation[res].property_details[0]
                  .prop_area_shape_cords == "string"
              ) {
                el.selectedreservation[
                  res
                ].property_details[0].prop_area_shape_cords = JSON.parse(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cords
                );
                ///////////////////////////////////
                ////////Change Coordinate/////////
                //////////////////////////////////
                var cords: any = {};
                el.selectedreservation[
                  res
                ].property_details[0].prop_area_shape_cord = [];
                for (
                  var x = 0;
                  x <
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cords.length;
                  x++
                ) {
                  try {
                    el.selectedreservation[
                      res
                    ].property_details[0].prop_area_shape_cords[
                      x
                    ] = el.selectedreservation[
                      res
                    ].property_details[0].prop_area_shape_cords[x].split(",");
                  } catch (e) {
                    // return;
                  }
                  cords = {
                    lat: parseFloat(
                      el.selectedreservation[res].property_details[0]
                        .prop_area_shape_cords[x][0]
                    ),
                    lng: parseFloat(
                      el.selectedreservation[res].property_details[0]
                        .prop_area_shape_cords[x][1]
                    )
                  };
                  el.selectedreservation[
                    res
                  ].property_details[0].prop_area_shape_cord.push(cords);
                  el.selectedreservation[
                    res
                  ].property_details[0].prop_area_shape_cords[x].lat =
                    el.selectedreservation[
                      res
                    ].property_details[0].prop_area_shape_cords[x][0];
                  el.selectedreservation[
                    res
                  ].property_details[0].prop_area_shape_cords[x].lng =
                    el.selectedreservation[
                      res
                    ].property_details[0].prop_area_shape_cords[x][1];
                }
                var notify = el.polygonlocation(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cord,
                  el.track.getlat(),
                  el.track.getlng()
                  // location.coords.latitude,
                  // location.coords.longitude
                );
                if (notify && el.auth.isOwner() == false) {
                  // el.insideproperty({
                  //   // latitude: el.track.getlat(),
                  //   // longitude: el.track.getlng(),
                  //   property: el.selectedreservation[res],
                  //   index: res
                  // });
                  var data = {
                    latitude: el.track.getlat(),
                    longitude: el.track.getlng(),
                    property: el.selectedreservation[res],
                    index: res
                  };
                  if (
                    this.auth.getpropertynotification(
                      data.property.booked_property_id
                    )
                  ) {
                    if (
                      this.sendnotification[data.index].upon_arival == "" ||
                      this.sendnotification[data.index].upon_arival == null ||
                      this.sendnotification[data.index].upon_arival == undefined
                    ) {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.auth.sendlocalnotification(
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                    } else {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.sendnotification[
                        data.index
                      ].upon_arival = this.sendnotification[
                        data.index
                      ].upon_arival.replace(/<\/?[^>]+>/gi, "");
                      this.auth.sendlocalnotification(
                        this.sendnotification[data.index].upon_arival
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        this.sendnotification[data.index].upon_arival
                      );
                    }
                  } else {
                    this.updatelocation(
                      data,
                      data.property,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    // this.startbackground();
                  }
                } else {
                }
              } else {
                var notify = el.polygonlocation(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cord,
                  el.track.getlat(),
                  el.track.getlng()
                );
                if (notify && el.auth.isOwner() == false) {
                  var data = {
                    latitude: el.track.getlat(),
                    longitude: el.track.getlng(),
                    property: el.selectedreservation[res],
                    index: res
                  };
                  if (
                    this.auth.getpropertynotification(
                      data.property.booked_property_id
                    )
                  ) {
                    if (
                      this.sendnotification[data.index].upon_arival == "" ||
                      this.sendnotification[data.index].upon_arival == null ||
                      this.sendnotification[data.index].upon_arival == undefined
                    ) {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.auth.sendlocalnotification(
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                    } else {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.sendnotification[
                        data.index
                      ].upon_arival = this.sendnotification[
                        data.index
                      ].upon_arival.replace(/<\/?[^>]+>/gi, "");
                      this.auth.sendlocalnotification(
                        this.sendnotification[data.index].upon_arival
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        this.sendnotification[data.index].upon_arival
                      );
                    }
                  } else {
                    this.updatelocation(
                      data,
                      data.property,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    // this.startbackground();
                  }
                } else {
                }
              }
            } else if (
              el.selectedreservation[res].property_details[0]
                .prop_area_shape_type == "rectangle"
            ) {
              if (
                typeof el.selectedreservation[res].property_details[0]
                  .prop_area_shape_cords == "string"
              ) {
                el.selectedreservation[
                  res
                ].property_details[0].prop_area_shape_cords = JSON.parse(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cords
                );
                var notify = el.rectanglelocation(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cords,
                  el.track.getlat(),
                  el.track.getlng()
                );
                if (notify && el.auth.isOwner() == false) {
                  var data = {
                    latitude: el.track.getlat(),
                    longitude: el.track.getlng(),
                    property: el.selectedreservation[res],
                    index: res
                  };
                  if (
                    this.auth.getpropertynotification(
                      data.property.booked_property_id
                    )
                  ) {
                    if (
                      (this.sendnotification == undefined &&
                        this.sendnotification[data.index] == undefined &&
                        this.sendnotification[data.index].upon_arival == "") ||
                      this.sendnotification[data.index].upon_arival == null ||
                      this.sendnotification[data.index].upon_arival == undefined
                    ) {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.auth.sendlocalnotification(
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                    } else {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.sendnotification[
                        data.index
                      ].upon_arival = this.sendnotification[
                        data.index
                      ].upon_arival.replace(/<\/?[^>]+>/gi, "");
                      this.auth.sendlocalnotification(
                        this.sendnotification[data.index].upon_arival
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        this.sendnotification[data.index].upon_arival
                      );
                    }
                  } else {
                    this.updatelocation(
                      data,
                      data.property,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    // this.startbackground();
                  }
                } else {
                }
              } else {
                var notify = el.rectanglelocation(
                  el.selectedreservation[res].property_details[0]
                    .prop_area_shape_cords,
                  el.track.getlat(),
                  el.track.getlng()
                );
                if (notify && el.auth.isOwner() == false) {
                  var data = {
                    latitude: el.track.getlat(),
                    longitude: el.track.getlng(),
                    property: el.selectedreservation[res],
                    index: res
                  };
                  if (
                    this.auth.getpropertynotification(
                      data.property.booked_property_id
                    )
                  ) {
                    if (
                      (this.sendnotification == undefined &&
                        this.sendnotification[data.index] == undefined &&
                        this.sendnotification[data.index].upon_arival == "") ||
                      this.sendnotification[data.index].upon_arival == null ||
                      this.sendnotification[data.index].upon_arival == undefined
                    ) {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.auth.sendlocalnotification(
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        "Hi you have reached your reserved property" +
                          " " +
                          data.property.property_details[0].post_title.replace(
                            /<\/?[^>]+>/gi,
                            ""
                          )
                      );
                    } else {
                      this.updatelocation(
                        data,
                        data.property,
                        el.track.getlat(),
                        el.track.getlng()
                      );
                      // this.startbackground();
                      this.sendnotification[
                        data.index
                      ].upon_arival = this.sendnotification[
                        data.index
                      ].upon_arival.replace(/<\/?[^>]+>/gi, "");
                      this.auth.sendlocalnotification(
                        this.sendnotification[data.index].upon_arival
                      );
                      this.auth.propertynotification(
                        data.property.booked_property_id
                      );
                      this.reachedproperty(
                        0,
                        this.sendnotification[data.index].upon_arival
                      );
                    }
                  } else {
                    this.updatelocation(
                      data,
                      data.property,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    // this.startbackground();
                  }
                } else {
                  // this.startbackground();
                }
              }
            }
            //////////////////////////////////////
            ////////////Slot Outline /////////////
            /////////////////////////////////////
            if (
              el.selectedreservation[res].array_slots_data &&
              el.selectedreservation[res].array_slots_data.length
            ) {
              for (
                var slot = 0;
                slot < el.selectedreservation[res].array_slots_data.length;
                slot++
              ) {
                if (
                  el.selectedreservation[res].array_slots_data[slot].id ==
                    el.selectedreservation[res].linked_slot_tothis_booking &&
                  el.selectedreservation[res].array_slots_data[slot]
                    .shapetype == "circle"
                ) {
                  var currentloc = new google.maps.Marker({
                    position: new google.maps.LatLng(
                      el.track.getlat(),
                      el.track.getlng()
                    ),
                    map: this.map_canvas
                  });
                  var proloc = new google.maps.Circle({
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.5,
                    strokeWeight: 2,
                    fillColor: "#FF0000",
                    fillOpacity: 0.2,
                    map: this.map_canvas,
                    center: new google.maps.LatLng(
                      el.auth.convertToint(
                        el.selectedreservation[res].array_slots_data[slot]
                          .slot_circle_lat
                      ),
                      el.auth.convertToint(
                        el.selectedreservation[res].array_slots_data[slot]
                          .slot_circle_lng
                      )
                    ),
                    radius: el.auth.convertToint(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_circle_radious
                    )
                  });
                  var notify: any = el.circlelocation(
                    currentloc.getPosition(),
                    proloc.getCenter(),
                    el.auth.convertToint(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_circle_radious
                    )
                  );
                  if (notify && el.auth.isOwner() == false) {
                    el.insideslot({
                      latitude: el.track.getlat(),
                      longitude: el.track.getlng(),
                      property: el.selectedreservation[res],
                      index: res
                    });
                  } else {
                  }
                } else if (
                  el.selectedreservation[res].array_slots_data[slot].id ==
                    el.selectedreservation[res].linked_slot_tothis_booking &&
                  el.selectedreservation[res].array_slots_data[slot]
                    .shapetype == "rectangle"
                ) {
                  if (
                    typeof el.selectedreservation[res].array_slots_data[slot]
                      .slot_coordinates == "string"
                  ) {
                    el.selectedreservation[res].array_slots_data[
                      slot
                    ].slot_coordinates = el.selectedreservation[
                      res
                    ].array_slots_data[slot].slot_coordinates.replace(
                      /\\/g,
                      ""
                    );
                    el.selectedreservation[res].array_slots_data[
                      slot
                    ].slot_coordinates = JSON.parse(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinates
                    );
                    var notify = el.rectanglelocation(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinates,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    if (notify && el.auth.isOwner() == false) {
                      el.insideslot({
                        latitude: el.track.getlat(),
                        longitude: el.track.getlng(),
                        property: el.selectedreservation[res],
                        index: res
                      });
                    } else {
                    }
                  } else {
                    var notify = el.rectanglelocation(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinates,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    if (notify && el.auth.isOwner() == false) {
                      el.insideslot({
                        latitude: el.track.getlat(),
                        longitude: el.track.getlng(),
                        property: el.selectedreservation[res],
                        index: res
                      });
                    } else {
                    }
                  }
                } else if (
                  el.selectedreservation[res].array_slots_data[slot].id ==
                    el.selectedreservation[res].linked_slot_tothis_booking &&
                  el.selectedreservation[res].array_slots_data[slot]
                    .shapetype == "polygon"
                ) {
                  if (
                    typeof el.selectedreservation[res].array_slots_data[slot]
                      .slot_coordinates == "string"
                  ) {
                    el.selectedreservation[res].array_slots_data[
                      slot
                    ].slot_coordinates = el.selectedreservation[
                      res
                    ].array_slots_data[slot].slot_coordinates.replace(
                      /\\/g,
                      ""
                    );
                    el.selectedreservation[res].array_slots_data[
                      slot
                    ].slot_coordinates = JSON.parse(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinates
                    );
                    ///////////////////////////////////
                    ////////Change Coordinate/////////
                    //////////////////////////////////
                    var cords: any = {};
                    el.selectedreservation[res].array_slots_data[
                      slot
                    ].slot_coordinate = [];
                    for (
                      var x = 0;
                      x <
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinates.length;
                      x++
                    ) {
                      try {
                        el.selectedreservation[res].array_slots_data[
                          slot
                        ].slot_coordinates[x] = el.selectedreservation[
                          res
                        ].array_slots_data[slot].slot_coordinates[x].split(",");
                      } catch (e) {
                        // return;
                      }
                      cords = {
                        lat: parseFloat(
                          el.selectedreservation[res].array_slots_data[slot]
                            .slot_coordinates[x][0]
                        ),
                        lng: parseFloat(
                          el.selectedreservation[res].array_slots_data[slot]
                            .slot_coordinates[x][1]
                        )
                      };
                      el.selectedreservation[res].array_slots_data[
                        slot
                      ].slot_coordinate.push(cords);
                      el.selectedreservation[res].array_slots_data[
                        slot
                      ].slot_coordinates[x].lat =
                        el.selectedreservation[res].array_slots_data[
                          slot
                        ].slot_coordinates[x][0];
                      el.selectedreservation[res].array_slots_data[
                        slot
                      ].slot_coordinates[x].lng =
                        el.selectedreservation[res].array_slots_data[
                          slot
                        ].slot_coordinates[x][1];
                    }
                    var notify = el.polygonlocation(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinate,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    if (notify && el.auth.isOwner() == false) {
                      el.insideslot({
                        latitude: el.track.getlat(),
                        longitude: el.track.getlng(),
                        property: el.selectedreservation[res],
                        index: res
                      });
                    } else {
                    }
                  } else {
                    var notify = el.polygonlocation(
                      el.selectedreservation[res].array_slots_data[slot]
                        .slot_coordinate,
                      el.track.getlat(),
                      el.track.getlng()
                    );
                    if (notify && el.auth.isOwner() == false) {
                      el.insideslot({
                        latitude: el.track.getlat(),
                        longitude: el.track.getlng(),
                        property: el.selectedreservation[res],
                        index: res
                      });
                    } else {
                    }
                  }
                }
              }
            }
          }
          if (el.auth.isLogin() == false) {
            if (el.notify) {
              clearInterval(el.notify);
            }
            if (el.watch) {
              el.geolocation.clearWatch(el.watch);
            }
          }
        } else {
          if (el.notify) {
            clearInterval(el.notify);
          }
          if (el.watch) {
            el.geolocation.clearWatch(el.watch);
          }
        }
      }
    }, 6000);
  }

  insideslot(data) {
    if (
      this.auth.getpropertyslotnotification(
        data.property.array_slots_data.find(
          x => x.id == data.property.linked_slot_tothis_booking
        ).id
      )
    ) {
      try {
        this.auth
          .getmessages2(
            this.auth.convertToint(data.property.booked_property_id),
            this.auth.getuserId(),
            data.property.array_slots_data.find(
              x => x.id == data.property.linked_slot_tothis_booking
            ).area_id
          )
          .subscribe(
            datas => {
              if (datas.json() && datas.json().status == 1) {
                if (
                  datas.json().result &&
                  datas.json().result[0] &&
                  datas.json().result[0].slot_p_n
                ) {
                  this.auth.sendlocalnotification(
                    datas.json().result[0].slot_p_n
                  );
                  this.auth.propertyslotnotification(
                    data.property.array_slots_data.find(
                      x => x.id == data.property.linked_slot_tothis_booking
                    ).id
                  );
                  this.reachedslot(0, datas.json().result[0].slot_p_n);
                } else {
                  this.auth.sendlocalnotification(
                    "Hi you have reached your reserved property slot" +
                      " " +
                      data.property.property_details[0].post_title.replace(
                        /<\/?[^>]+>/gi,
                        ""
                      )
                  );
                  data.property;
                  this.auth.propertyslotnotification(
                    data.property.array_slots_data.find(
                      x => x.id == data.property.linked_slot_tothis_booking
                    ).id
                  );
                  this.reachedslot(
                    0,
                    "Hi you have reached your reserved property slot" +
                      " " +
                      data.property.property_details[0].post_title.replace(
                        /<\/?[^>]+>/gi,
                        ""
                      )
                  );
                }
              }
            },
            errorHandler => {
              this.auth.stoploader();
              this.auth.errtoast(errorHandler);
            }
          );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    }
    return;
  }
  insideproperty(data) {
    if (this.auth.getpropertynotification(data.property.booked_property_id)) {
      if (
        (this.sendnotification == undefined &&
          this.sendnotification[data.index] == undefined &&
          this.sendnotification[data.index].upon_arival == "") ||
        this.sendnotification[data.index].upon_arival == null ||
        this.sendnotification[data.index].upon_arival == undefined
      ) {
        // this.updatelocation(data, data.property);
        this.startbackground();
        this.auth.sendlocalnotification(
          "Hi you have reached your reserved property" +
            " " +
            data.property.property_details[0].post_title.replace(
              /<\/?[^>]+>/gi,
              ""
            )
        );
        this.auth.propertynotification(data.property.booked_property_id);
        this.reachedproperty(
          0,
          "Hi you have reached your reserved property" +
            " " +
            data.property.property_details[0].post_title.replace(
              /<\/?[^>]+>/gi,
              ""
            )
        );
      } else {
        this.sendnotification[data.index].upon_arival = this.sendnotification[
          data.index
        ].upon_arival.replace(/<\/?[^>]+>/gi, "");
        // this.updatelocation(data, data.property);
        this.auth.sendlocalnotification(
          this.sendnotification[data.index].upon_arival
        );
        this.auth.propertynotification(data.property.booked_property_id);
        this.reachedproperty(0, this.sendnotification[data.index].upon_arival);
      }
      // this.auth.enablebackground();
    } else {
      // this.updatelocation(data, data.property);
    }
    return;
  }

  /////////////////////////////////////////
  /////////////Reached Property////////////
  ////////////////////////////////////////
  backactive: boolean = false;
  reachedproperty(type, message) {
    this.backactive = true;
    var slotModal: any = this.modalCtrl.create(
      "SendpushPage",
      {
        type: type,
        message: message
      },
      { cssClass: "reachmodal" }
    );
    slotModal.present();
    slotModal.onDidDismiss(data => {
      this.backactive = false;
    });
    return;
  }
  reachedslot(type, message) {
    message = message.replace(/<\/?[^>]+>/gi, "");
    this.backactive = true;
    var slotModal: any = this.modalCtrl.create(
      "SendpushPage",
      {
        type: type,
        message: message
      },
      { cssClass: "reachmodal" }
    );
    slotModal.present();
    slotModal.onDidDismiss(data => {
      this.backactive = false;
    });
  }

  /////////////////////////////////////////
  ////////User out of the Property/////////
  ////////////////////////////////////////
  goout(booked_property_id) {
    // this.logintokenuser = this.fcmp.checkusers().snapshotChanges();
    // this.logintokenuserSubscribe = this.logintokenuser.subscribe(
    //   queriedItems => {
    //     queriedItems.forEach(action => {
    //       this.fcmp.updateGeolocation2(action.key, booked_property_id);
    //       this.pleaseunsubscribe();
    //     });
    //   }
    // );
  }

  ///////////////////////////
  ///Inside a property///////
  ///////////////////////////
  circlelocation(usercurrentlocation, locationcenter, radius) {
    if (
      google.maps.geometry.spherical.computeDistanceBetween(
        usercurrentlocation,
        locationcenter
      ) <= radius
    ) {
      return true;
    } else {
      return false;
    }
  }
  rectanglelocation(reservation, currentlat, currentlng) {
    if (reservation && currentlat && currentlng) {
      var rectangle;
      rectangle = new google.maps.Rectangle({
        bounds: reservation[0],
        editable: true,
        draggable: true,
        geodesic: true,
        map: this.map_canvas
      });
      return rectangle.getBounds().contains({
        lat: currentlat,
        lng: currentlng
      });
    } else {
      return false;
    }
  }
  polygonlocation(reservation, currentlat, currentlng) {
    if (reservation && currentlat && currentlng) {
      return google.maps.geometry.poly.containsLocation(
        new google.maps.LatLng(currentlat, currentlng),
        new google.maps.Polygon({
          paths: reservation
        })
      )
        ? true
        : false;
    } else {
      return false;
    }
  }

  ////////////////////////////////////////////
  /////////Update Location in Firebase ///////
  ///////////////////////////////////////////
  firedata2 = firebase.database().ref("/doveblaster_data");
  updatelocation(data, selectedreservation, lat, lng) {
    // debugger;
    var tiss: any = this;
    tiss.logintoken = tiss.fcmp.checkusers().valueChanges();
    tiss.logintokenSubscribe = tiss.logintoken
      .first()
      .subscribe(queriedItems => {
        if (queriedItems.length == 0) {
          this.logintoken = this.fcmp.checkusers().valueChanges();
          this.logintokenSubscribe = this.logintoken.subscribe(queriedItems => {
            if (queriedItems.length == 0) {
              this.fcmp.getToken();
            } else {
              this.logintokenuser = this.fcmp.checkusers().snapshotChanges();
              this.logintokenuserSubscribe = this.logintokenuser.subscribe(
                queriedItems => {
                  queriedItems.forEach(action => {
                    this.fcmp.updateToken(action.key);
                  });
                }
              );
            }
            if (this.auth.isLogin() == false) {
              this.logintokenSubscribe.unsubscribe();
              this.logintokenuserSubscribe.unsubscribe();
            } else {
            }
          });
        } else {
          tiss.logintokenuser = tiss.fcmp.checkusers().snapshotChanges();
          tiss.logintokenuserSubscribe = tiss.logintokenuser
            .first()
            .subscribe(queriedItems => {
              queriedItems.forEach(action => {
                tiss.fcmp.updateGeolocation(
                  action.key,
                  tiss.track.getlat(),
                  tiss.track.getlng(),
                  selectedreservation.booked_property_id
                );
                tiss.pleaseunsubscribe();
              });
            });
        }
        if (tiss.auth.isLogin() == false) {
          tiss.pleaseunsubscribe();
        }
      });
    // this.insidepropertyid = selectedreservation.booked_property_id;
    // tiss.firedata2
    //   .orderByChild("token")
    //   .equalTo(tiss.fcmp.getfiretoken())
    //   .once("value", function(snapshot) {
    //     if (snapshot.numChildren() > 1) {
    //       var db = firebase.database();
    //       db.ref("doveblaster_data/" + data.key).remove();
    //       tiss.auth.toast("Session Expired. Please login again");
    //       tiss.auth.logout();
    //       tiss.navCtrl.setRoot("LoginPage");
    //     }
    //     snapshot.forEach(function(data) {
    //       var db = firebase.database();
    //       db.ref("doveblaster_data/" + data.key).update({
    //         lat: lat,
    //         lng: lng,
    //         datetime: tiss.datepipe.transform(new Date(), "yyyy-MM-dd h:mm a"),
    //         propertyId: tiss.auth.getpropertyId(),
    //         userName: tiss.auth.getUsername("username"),
    //         token: tiss.auth.getuserToken(),
    //         userId: tiss.auth.getuserId()
    //       });
    //     });
    //     tiss.firedata2
    //       .orderByChild("token")
    //       .equalTo(tiss.fcmp.getfiretoken())
    //       .off("value");
    //   });

    // tiss.logintoken = tiss.fcmp.checkusers().valueChanges();
    // tiss.logintokenSubscribe = tiss.logintoken
    //   .first()
    //   .subscribe(queriedItems => {
    //     if (queriedItems.length == 0) {
    //     } else {
    //       tiss.logintokenuser = tiss.fcmp.checkusers().snapshotChanges();
    //       tiss.logintokenuserSubscribe = tiss.logintokenuser
    //         .first()
    //         .subscribe(queriedItems => {
    //           queriedItems.forEach(action => {
    //             tiss.fcmp.updateGeolocation(
    //               action.key,
    //               tiss.track.getlat(),
    //               tiss.track.getlng(),
    //               selectedreservation.booked_property_id
    //             );
    //             tiss.pleaseunsubscribe();
    //           });
    //         });
    //     }
    //   if (tiss.auth.isLogin() == false) {
    //     tiss.pleaseunsubscribe();
    //   }
    // });
  }
  circlecalculatedistanceslot(
    radius,
    userlat,
    userlng,
    propertylat,
    propertylng
  ) {
    if (userlat == propertylat && userlng == propertylng) {
      return 0;
    } else {
      var ky = 40000 / 360;
      var kx = Math.cos((Math.PI * propertylat) / 180.0) * ky;
      var dx = Math.abs(propertylng - userlng) * kx;
      var dy = Math.abs(propertylat - userlat) * ky;
      return Math.sqrt(dx * dx + dy * dy) <= radius;
    }
  }
  calculatedistanceslot(area, userlat, userlng, propertylat, propertylng) {
    if (userlat == propertylat && userlng == propertylng) {
      return 0;
    } else {
      var radius = Math.sqrt(area / 247.105 / Math.PI);
      var ky = 40000 / 360;
      var kx = Math.cos((Math.PI * propertylat) / 180.0) * ky;
      var dx = Math.abs(propertylng - userlng) * kx;
      var dy = Math.abs(propertylat - userlat) * ky;
      return Math.sqrt(dx * dx + dy * dy) <= radius;
    }
  }
  calculatedistance1(
    type,
    area,
    userlat,
    userlng,
    propertylat,
    propertylng,
    unit
  ) {
    if (userlat == propertylat && userlng == propertylng) {
      return 0;
    } else {
      var radius = Math.sqrt(area / 247.105 / Math.PI);
      var ky = 40000 / 360;
      var kx = Math.cos((Math.PI * propertylat) / 180.0) * ky;
      var dx = Math.abs(propertylng - userlng) * kx;
      var dy = Math.abs(propertylat - userlat) * ky;
      return Math.sqrt(dx * dx + dy * dy) <= radius;
      // if (type == "rectangle") {
      //   this.selectedreservation.property_details.prop_area_shape_cords = JSON.parse(
      //     this.selectedreservation.property_details.prop_area_shape_cords
      //   );
      // }
      // if (type == "circle") {
      // }
    }
  }
  arePointsNear(userlat, userlng, propertylat, propertylng, radius) {}
  //:::    unit = the unit you desire for results                               :::
  //:::           where: 'M' is statute miles (default)                         :::
  //:::                  'K' is kilometers                                      :::
  //:::                  'N' is nautical miles
  //:::                  'A' is Acres
  calculatedistance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") {
        dist = dist * 1.609344;
      }
      if (unit == "N") {
        dist = dist * 0.8684;
      }
      if (unit == "A") {
        dist = dist * 12;
      }
      return dist;
    }
  }

  // selectedreservation: any;
  selectedreservation: any = [];
  myreservations() {
    this.selectedreservation = [];
    try {
      this.auth.reservation(this.auth.getuserId(), 1, 5).subscribe(
        // this.auth.getuserReservation(this.auth.getuserId(), 1, 5).subscribe(
        data => {
          if (data.json() && data.json().status == 1) {
            if (
              data.json().user_reservation_data &&
              data.json().user_reservation_data.length
            ) {
              for (
                var dt = 0;
                dt < data.json().user_reservation_data.length;
                dt++
              ) {
                if (
                  data.json().user_reservation_data[dt].booking_from_date ==
                    this.auth.changeformat1(new Date()) &&
                  data.json().user_reservation_data[dt].booking_status !=
                    "canceled"
                ) {
                  this.selectedreservation.push(
                    data.json().user_reservation_data[dt]
                  );
                } else {
                }
                if (dt == data.json().user_reservation_data.length - 1) {
                  setTimeout(() => {
                    // debugger;
                    this.getlocation();
                  }, 2000);
                }
                if (
                  dt == data.json().user_reservation_data.length - 1 &&
                  this.selectedreservation.length
                ) {
                  this.selectedreservation.forEach((item, i) => {
                    this.getmessage(item.booked_property_id, i);
                  });
                }
              }
            }
          }
        },
        errorHandler => {
          // this.auth.stoploader();
          // this.auth.errtoast(errorHandler);
          // this.confirmAlertreser();
          this.auth.setautorelogin(this.relogintry);
          this.relogintry++;
          if (this.relogintry > 5) {
            this.confirmAlertreser();
          }
          this.gologin(
            this.auth.getlogincredentials().username,
            this.auth.getlogincredentials().password
          );
        }
      );
    } catch (err) {
      // this.auth.stoploader();
      // this.auth.errtoast(err);
      // this.confirmAlertreser();
      this.auth.setautorelogin(this.relogintry);
      this.relogintry++;
      if (this.relogintry > 5) {
        this.confirmAlertreser();
      }
      this.gologin(
        this.auth.getlogincredentials().username,
        this.auth.getlogincredentials().password
      );
    }
  }

  confirmAlertreser() {
    let alert = this.alertCtrl.create({
      title: "Try again",
      message: "Not able to get reservation details !",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.myreservations();
          }
        }
      ]
    });
    alert.present();
  }

  confirmAlertproperties() {
    let alert = this.alertCtrl.create({
      title: "Try again",
      message: "Not able to get properties !",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.getallpropertyformap();
          }
        }
      ]
    });
    alert.present();
  }

  confirmAlertpropertiesl() {
    let alert = this.alertCtrl.create({
      title: "Try again",
      message: "Not able to get properties listing!",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.getallproperty();
          }
        }
      ]
    });
    alert.present();
  }

  pleaseunsubscribe() {
    if (this.logintokenSubscribe) {
      this.logintokenSubscribe.unsubscribe();
    }
    if (this.logintokenuserSubscribe) {
      this.logintokenuserSubscribe.unsubscribe();
    }
  }
  watchingposition;
  pleaseclearwatch() {}
  insidepropertyid: any;
  isinside: boolean;
  startbackground() {
    // var tis = this;
    // this.platform.ready().then(() => {
    // BackgroundGeolocation.onLocation(location => {
    //   console.log("[location] - ", location);
    //   BackgroundGeolocation.setConfig({
    //     params: {
    //       device_id: "abc123",
    //       random_num: Math.random(),
    //       token: this.fcmp.checktoken()
    //     }
    //   });
    // });
    // let bgGeo = (<any>window).BackgroundGeolocation;
    // BackgroundGeolocation.onMotionChange(event => {
    //   console.log("[motionchange] - ", event.isMoving, event.location);
    //   bgGeo.on("location", location => {
    //     console.log("[location] - ", location);
    //   });
    // });
    // BackgroundGeolocation.onHttp(response => {
    //   console.log(
    //     "[http] - ",
    //     response.success,
    //     response.status,
    //     response.responseText
    //   );
    // });
    // BackgroundGeolocation.onProviderChange(event => {
    //   console.log(
    //     "[providerchange] - ",
    //     event.enabled,
    //     event.status,
    //     event.gps
    //   );
    // });
    // BackgroundGeolocation.getLog(function(log) {
    //   console.log(log);
    // });
    // 2.  Configure the plugin with #ready
    // if (this.insidepropertyid) {
    //   debugger;
    //   console.log("Called Background");
    //   BackgroundGeolocation.ready(
    //     {
    //       reset: true,
    //       debug: false,
    //       // logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
    //       desiredAccuracy: 0,
    //       desiredOdometerAccuracy: 1,
    //       disableElasticity: true,
    //       distanceFilter: 0,
    //       elasticityMultiplier: 0,
    //       // foregroundService: true,
    //       url: "https://dayleasing-958cf.firebaseio.com/dayleasingdata.json",
    //       httpRootProperty: "data",
    //       params: {
    //         propertyid: 1,
    //         // propertyid: tis.insidepropertyid,
    //         userid: tis.auth.getuserId(),
    //         token: tis.fcmp.checktoken(),
    //         datetime: new Date().getTime(),
    //         userName: tis.auth.getUsername("username")
    //       },
    //       locationTemplate: '{ "lat":<%= latitude %>, "lng":<%= longitude %> }',
    //       autoSync: true,
    //       stopOnTerminate: false,
    //       startOnBoot: true
    //     },
    //     state => {
    //       console.log("[ready] BackgroundGeolocation is ready to use");
    //       if (!state.enabled) {
    //         // 3.  Start tracking.
    //         BackgroundGeolocation.start();
    //       } else {
    //         BackgroundGeolocation.stop();
    //         tis.db.list("/").remove();
    //       }
    //     }
    //   );
    //   // }
    // });
  }
}
