import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddnewpropertyPage } from "./addnewproperty";
import { AutoCompleteModule } from "ionic2-auto-complete";

@NgModule({
  declarations: [AddnewpropertyPage],
  imports: [AutoCompleteModule, IonicPageModule.forChild(AddnewpropertyPage)]
})
export class AddnewpropertyPageModule {}
