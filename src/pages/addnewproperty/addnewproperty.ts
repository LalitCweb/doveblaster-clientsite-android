import {
  Component,
  OnInit,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import { IonicPage, NavController, NavParams, Platform } from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Http } from "@angular/http";
import { AuthProvider } from "../../providers/auth/auth";
import { CompleteTestServiceProvider } from "../../providers/complete-test-service/complete-test-service";
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderForwardResult,
  NativeGeocoderOptions
} from "@ionic-native/native-geocoder";

@IonicPage()
@Component({
  selector: "page-addnewproperty",
  templateUrl: "addnewproperty.html"
})
export class AddnewpropertyPage implements OnInit {
  @ViewChild("map_canvas") mapElements: ElementRef;
  // service = new google.maps.places.AutocompleteService();
  service: any;
  placesService: any;
  configs: FormGroup;
  property: any = {
    title: "",
    category: "",
    city: "",
    wantlease: "",
    county: "",
    state: "",
    country: "",
    zipcode: "",
    description: "",
    feature: ""
  };
  autocompleteItems;
  autocomplete;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    private zone: NgZone,
    public auth: AuthProvider,
    private nativeGeocoder: NativeGeocoder,
    public completeTestService: CompleteTestServiceProvider,
    public platform: Platform
  ) {
    this.platform.ready().then(() => {
      this.service = new google.maps.places.AutocompleteService();
    });
  }
  ngOnInit() {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ""
    };
    this.configs = new FormGroup({
      title: new FormControl("", [Validators.required]),
      category: new FormControl("", [Validators.required]),
      city: new FormControl("", [Validators.required]),
      wantlease: new FormControl("", [Validators.required]),
      county: new FormControl("", [Validators.required]),
      state: new FormControl("", [Validators.required]),
      country: new FormControl("", [Validators.required]),
      zipcode: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      feature: new FormControl("", [Validators.required])
    });
  }
  ionViewDidLoad() {}
  map_canvas;
  ionViewWillEnter() {
    this.initializemap();
    this.getcategories();
    this.getlease();
    this.getfeatures();
  }

  initializemap() {
    this.platform.ready().then(() => {
      let mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(22, 77),
        tilt: 0
      };
      this.map_canvas = new google.maps.Map(
        this.mapElements.nativeElement,
        mapOptions
      );
      this.placesService = new google.maps.places.PlacesService(
        this.map_canvas
      );
    });
  }
  categories: any = [];
  getcategories() {
    this.auth.getcategories(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().all_categories) {
          this.categories = data.json().all_categories;
        }
      }
    });
  }
  features: any;
  getfeatures() {
    this.auth.getfeatures(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().eminities) {
          this.features = data.json().eminities;
        }
      }
    });
  }
  lease: any = [];
  getlease() {
    this.auth.getlease(this.auth.getuserId()).subscribe(data => {
      if (data) {
        if (data.json().all_action_categories) {
          this.lease = data.json().all_action_categories;
        }
      }
    });
  }

  goto() {}
  citysearch(data) {
    if (this.property.city == "") {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions(
      {
        input: this.property.city
      },
      (predictions, status) => {
        me.autocompleteItems = [];

        me.zone.run(() => {
          if (predictions != null) {
            predictions.forEach(prediction => {
              me.autocompleteItems.push(prediction);
            });
          }
        });
      }
    );
  }

  public propertylocation: any = {};
  chooseItem(data) {
    this.filter(data.place_id);
    // if (data.terms && data.terms.length) {
    //   this.property.country = data.terms[data.terms.length - 1].value;
    // }
    // this.property.state = data.structured_formatting.main_text;
    // this.property.city = data.description;
    debugger;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ placeId: data.place_id }, function(results, status) {
      debugger;
    });

    this.property.property_admin_area =
      data.structured_formatting.secondary_text;
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.nativeGeocoder
      .forwardGeocode(data.description, options)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        this.propertylocation.prop_area_center_lat = coordinates[0].latitude;
        this.propertylocation.prop_area_center_lng = coordinates[0].longitude;
      })
      .catch((error: any) => {
        this.auth.errtoast(error);
      });

    this.nativeGeocoder
      .reverseGeocode(
        this.propertylocation.prop_area_center_lat,
        this.propertylocation.prop_area_center_lng,
        options
      )
      .then((result: NativeGeocoderReverseResult[]) => {})
      .catch((error: any) => {});
    this.autocompleteItems = [];
  }

  filter(value) {
    var el = this;
    el.placesService.getDetails(
      {
        placeId: value
      },
      (details: any) => {
        this.zone.run(() => {
          for (var srch = 0; srch < details.address_components.length; srch++) {
            if (details.address_components[srch].types[0] == "locality") {
              this.property.city = details.address_components[srch].long_name;
            } else if (details.address_components[srch].types[0] == "country") {
              this.property.country =
                details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_2"
            ) {
              this.property.state = details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] ==
              "administrative_area_level_1"
            ) {
              this.property.state = details.address_components[srch].long_name;
            } else if (
              details.address_components[srch].types[0] == "postal_code"
            ) {
              this.property.zipcode =
                details.address_components[srch].long_name;
            }
          }
        });
      }
    );
  }

  submitproperty() {
    this.auth.startloader();
    try {
      var senddata = {
        user_id: this.auth.getuserId(),
        wpestate_title: this.property.title,
        prop_category: this.property.category,
        prop_action_category: this.property.wantlease,
        property_country: this.property.country,
        property_city: this.configs.value.city,
        property_admin_area: this.property.property_admin_area,
        prop_area_center_lat: this.propertylocation.prop_area_center_lat,
        prop_area_center_lng: this.propertylocation.prop_area_center_lng,
        property_county: this.property.county,
        property_state: this.property.state,
        property_zip: this.property.zipcode,
        property_description: this.property.description,
        checkboxes_eminities: this.property.feature,
        property_id: ""
      };
      this.auth
        .createprop1(
          this.auth.getuserId(),
          this.property,
          this.configs.value,
          this.propertylocation,
          this.emnities.checkboxes_eminities
        )
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              if (data.json().property_id) {
                senddata.property_id = data.json().property_id;
                this.navCtrl.push("AddareaPage", {
                  property_data: senddata
                });
              }
            } else if (data.json().status == 0) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
              } else {
                this.auth.toast("Currently not able to create property");
              }
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        });
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  emnities: any = {};
  addproperty() {
    if (this.configs.valid) {
      this.property.feature = [];
      var tis = this;
      tis.emnities.checkboxes_eminities = {};
      if (
        this.property.available_species &&
        this.property.available_species.length
      ) {
        this.property.available_species.forEach(function(species, index) {
          tis.property.feature.push(species);
        });
      }
      if (this.property.features && this.property.features.length) {
        this.property.features.forEach(function(features, index) {
          tis.property.feature.push(features);
        });
      }
      if (
        this.property.permitted_onproperty &&
        this.property.permitted_onproperty.length
      ) {
        this.property.permitted_onproperty.forEach(function(
          permittedonproperty,
          index
        ) {
          tis.property.feature.push(permittedonproperty);
        });
      }
      if (
        this.property.permitted_weapon &&
        this.property.permitted_weapon.length
      ) {
        this.property.permitted_weapon.forEach(function(
          permittedweapon,
          index
        ) {
          tis.property.feature.push(permittedweapon);
        });
      }

      this.features.available_species.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.available_species);
      });
      this.features.permitted_onproperty.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.permitted_onproperty);
      });
      this.features.permitted_weapon.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.permitted_weapon);
      });
      this.features.property_features.forEach((value, index) => {
        tis.emnities.checkboxes_eminities[
          tis.auth.capitalize(value)
        ] = tis.checkemnities(value, tis.property.features);
      });

      this.submitproperty();
    } else {
      this.auth.toast("Please Fill all mandatory fields");
    }
  }

  checkemnities(value, dataarr) {
    if (dataarr && value) {
      if (dataarr.filter(item => item == value).length == 0) {
        return "0";
      } else {
        return "1";
      }
    } else {
      return "0";
    }
  }
}
