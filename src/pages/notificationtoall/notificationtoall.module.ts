import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationtoallPage } from './notificationtoall';

@NgModule({
  declarations: [
    NotificationtoallPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationtoallPage),
  ],
})
export class NotificationtoallPageModule {}
