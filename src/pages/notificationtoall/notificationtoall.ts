import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFirestore, QuerySnapshot } from "angularfire2/firestore";
import { AuthProvider } from "../../providers/auth/auth";
import { FcmProvider } from "../../providers/fcm/fcm";
/**
 * Generated class for the NotificationtoallPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-notificationtoall",
  templateUrl: "notificationtoall.html"
})
export class NotificationtoallPage {
  constructor(
    public navCtrl: NavController,
    public fcmp: FcmProvider,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad NotificationtoallPage");
  }

  message: any;
  sendnotification() {
    if (this.message) {
      this.auth.startloader();
      var tkn = this;
      var dev: any = this.fcmp.checkallusers().valueChanges();
      var devsubs = dev.subscribe(queriedItems => {
        var userst: any = [];
        var users: any = [];
        queriedItems.forEach(function(tokens, index) {
          if (userst.includes(tokens.token)) {
          } else {
            users.push(tokens);
            userst.push(tokens.token);
          }
        });
        users.forEach(function(tokens, index) {
          console.log(tokens.userId);
          tkn.sendNotificationall(
            queriedItems.length,
            index,
            tokens,
            tkn.message
          );
          if (index == users.length - 1) {
            this.auth.stoploader();
            if (devsubs) {
              devsubs.unsubscribe();
            }
          }
        });
        setTimeout(() => {
          this.auth.stoploader();
        }, 8000);
      });
    } else {
      this.auth.toast("Cannot send empty notification");
    }
  }

  title: any;
  body: any;
  sendNotificationall(item, index, token, body) {
    if (body) {
      try {
        this.auth.sendpushnotification(token.token, body).subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              this.title = "";
              this.body = "";
              this.message = "";
              var dataa: any = data.json();
              if (
                (dataa.success == 1 && index == item - 1) ||
                (dataa.success == 1 && index == 0)
              ) {
                this.auth.toast("Notification send successfully");
              } else if (index == item - 1) {
                this.auth.toast("Not able to send notification");
              }
            }
          },
          errorHandler => {
            this.auth.stoploader();
            this.auth.errtoast(errorHandler);
            // this.auth.logout();
            // this.navCtrl.setRoot("LoginPage");
          }
        );
      } catch (err) {
        this.auth.stoploader();
        this.auth.errtoast(err);
      }
    } else {
      this.auth.toast("Please type your message");
    }
  }
}
