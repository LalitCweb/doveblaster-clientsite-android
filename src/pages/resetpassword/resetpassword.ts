import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";

/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-resetpassword",
  templateUrl: "resetpassword.html"
})
export class ResetpasswordPage implements OnInit {
  // Validation form group definition
  configs: FormGroup;
  user: any = { password: "", cpassword: "" };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController
  ) {
    // Disable Side Menu
    this.menuCtrl.enable(false, "myMenu");
  }

  ngOnInit() {
    // Validations Definition
    this.configs = new FormGroup({
      password: new FormControl("", [Validators.required]),
      cpassword: new FormControl("", [Validators.required])
    });
  }
  ionViewDidLoad() {}
  resetPass() {
    if (this.user.password == this.user.cpassword) {
      if (this.configs.valid) {
        this.navCtrl.push("PasswordassistancePage", { page: "true" });
      }
    } else {
      alert(
        "The two passwords you entered do not match. Please re-enter and try again"
      );
    }
  }

  mismatch: any;
  pwdMatchValidator() {
    if (this.user.password != "" && this.user.cpassword != "") {
      if (
        this.user.password != this.user.cpassword ||
        this.user.cpassword != this.user.password
      ) {
        this.mismatch = true;
      } else {
        this.mismatch = false;
      }
    } else {
      this.mismatch = false;
    }
  }
}
