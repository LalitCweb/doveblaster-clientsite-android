import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditmediaPage } from './editmedia';

@NgModule({
  declarations: [
    EditmediaPage,
  ],
  imports: [
    IonicPageModule.forChild(EditmediaPage),
  ],
})
export class EditmediaPageModule {}
