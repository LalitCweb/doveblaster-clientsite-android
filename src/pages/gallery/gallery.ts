import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  ModalController
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-gallery",
  templateUrl: "gallery.html"
})
export class GalleryPage {
  video: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    private viewCtrl: ViewController,
    public modalCtrl: ModalController
  ) {
    this.media = this.navParams.get("medias");
    if (this.navParams.get("video")) {
      this.video = this.navParams.get("video");
    }
  }
  media: any;
  ionViewDidLoad() {}

  goto(page, media, index) {
    let mediaModal = this.modalCtrl.create(page, {
      medias: media,
      initial: index,
      video: this.video
    });
    mediaModal.present();

    mediaModal.onDidDismiss(data => {
      if (data.medias) {
        this.media = data.medias;
      }
      if (data.video) {
        this.video = data.video;
        this.makeurl();
      }
    });
    this.goback();
  }
  makeurl() {
    if (this.video.embed_video_type === "youtube") {
      this.video.embed_video_id = this.sanitizer.bypassSecurityTrustResourceUrl(
        "https://www.youtube.com/embed/" + this.video.embed_video_id
      );
    } else if (this.video.embed_video_type === "vimeo") {
      this.video.embed_video_id = this.sanitizer.bypassSecurityTrustResourceUrl(
        "https://vimeo.com/" + this.video.embed_video_id
      );
    }
  }

  goback() {
    this.viewCtrl.dismiss({
      medias: this.media,
      video: this.navParams.get("video")
    });
  }
}
