import { Component, OnInit, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  Content
} from "ionic-angular";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the SignuplandownerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-signuplandowner",
  templateUrl: "signuplandowner.html"
})
export class SignuplandownerPage implements OnInit {
  // Validation form group definition
  @ViewChild(Content)
  content: Content;
  configs: FormGroup;
  user: any = {
    email: "",
    password: "",
    cpassword: "",
    fname: "",
    lname: "",
    cname: "",
    term: "",
    agree: ""
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public auth: AuthProvider
  ) {
    // Disable Side Menu
    this.menuCtrl.enable(false, "myMenu");
  }

  ngOnInit() {
    // Validations Definition
    let EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.configs = new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(EMAILPATTERNS)
      ]),
      password: new FormControl("", [Validators.required]),
      cpassword: new FormControl("", [Validators.required]),
      fname: new FormControl("", [Validators.required]),
      lname: new FormControl("", [Validators.required]),
      cname: new FormControl("", [
        Validators.required,
        Validators.pattern("^(?=.*[a-zA-Z])[a-zA-Z0-9]+$")
      ]),
      term: new FormControl("", [Validators.required]),
      agree: new FormControl("", [Validators.required])
    });
  }

  ionViewDidLoad() {}

  ionViewWillLeave() {
    this.auth.clearcookie();
  }
  enter() {
    this.navCtrl.setRoot("LoginPage");
  }

  goto() {
    this.navCtrl.push("VerifyemailPage");
  }

  signup() {
    if (this.configs.valid) {
      if (
        this.user.term == false ||
        this.user.term == undefined ||
        this.user.term == null
      ) {
        this.auth.toast("You must to agree with terms and conditions!");
        return;
      }
      if (
        this.user.agree == false ||
        this.user.agree == undefined ||
        this.user.agree == null
      ) {
        this.auth.toast("You must to agree that you are legal property owner!");
        return;
      }

      if (
        this.user.password != this.user.cpassword ||
        this.user.cpassword != this.user.password
      ) {
        this.auth.toast("Passwords do not match!");
        return;
      }
      this.signuplandowner();
    } else {
      for (let i in this.configs.controls)
        this.configs.controls[i].markAsTouched();
      this.auth.toast("Form is invalid. Please validate form carefully");
      this.content.scrollToTop();
    }
  }
  signuplandowner() {
    this.auth.startloader();
    try {
      this.auth.signup(this.configs.value, 0).subscribe(
        data => {
          this.auth.stoploader();
          if (data.json() && data.json().register) {
            if (data.json().register == true) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
                this.navCtrl.push("VerifyemailPage", {
                  user_id: data.json().user_id,
                  userpass: {
                    username: this.configs.value.cname,
                    password: this.configs.value.password
                  }
                });
                this.configs.reset;
              } else {
                this.auth.toast("The account was created.You can login now.");
                this.configs.reset;
              }
            } else if (data.json().register == false) {
              if (data.json().message) {
                this.auth.toast(data.json().message);
              } else {
                this.auth.toast("Cannot create profile with these details");
              }
            }
          } else {
            if (data.json().message) {
              this.auth.toast(data.json().message);
              this.configs.reset;
            } else {
              this.auth.toast("Problem creating user.");
              this.configs.reset;
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          this.auth.toast("Currently not able to signup.");
        }
      );
    } catch (err) {
      this.auth.stoploader();
      this.auth.errtoast(err);
    }
  }
  getdetail() {
    this.auth.getuserdata(this.configs.value.cname).subscribe(
      data => {
        this.auth.clearcookie();
      },
      errorHandler => {
        this.auth.stoploader();
        this.auth.toast("Not able to get user data");
      }
      // () => {
      //   this.auth.stoploader();
      // }
    );
  }
}
