import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandownersPage } from './landowners';

@NgModule({
  declarations: [
    LandownersPage,
  ],
  imports: [
    IonicPageModule.forChild(LandownersPage),
  ],
})
export class LandownersPageModule {}
