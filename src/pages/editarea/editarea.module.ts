import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditareaPage } from './editarea';

@NgModule({
  declarations: [
    EditareaPage,
  ],
  imports: [
    IonicPageModule.forChild(EditareaPage),
  ],
})
export class EditareaPageModule {}
