import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the KeywordsrchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-keywordsrch",
  templateUrl: "keywordsrch.html"
})
export class KeywordsrchPage {
  serachdata: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider
  ) {
    if (typeof this.navParams.get("searchdata") != "string") {
      this.serachdata = this.navParams.get("searchdata");
    } else {
      this.serachdata = [];
    }
  }

  ionViewDidLoad() {}

  redirecto(data) {
    if (data.post_type == "estate_property") {
      this.locatetoproperty(data);
    } else if (data.post_type == "page") {
      this.taketomore(null, data.ID, "page");
    }
  }

  /////////////////////////////
  ///////Property Page////////
  ////////////////////////////
  locatetoproperty(pro) {
    this.auth.startloader();
    try {
      this.auth.getsinglealldetail(pro.ID, this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data && data.json() && data.json().status == 1) {
              var dt: any = data.json().events;
              dt.ID = pro.ID;
              this.navCtrl.push("PropertiesdetailPage", {
                property_data: dt,
                property_detail: true
              });
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            }
          }
        },
        errorHandler => {
          this.auth.stoploader();
          // this.auth.errtoast(errorHandler);
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
        }
      );
    } catch (e) {
      this.auth.stoploader();
      // this.auth.toast(e);
    }
  }

  /////////////////////////////
  ///////////More Page////////
  ////////////////////////////
  taketomore(menu_item_id, object_id, object) {
    this.auth.startloader();
    try {
      this.auth
        .getmoredetail(this.auth.getuserId(), menu_item_id, object_id, object)
        .subscribe(
          data => {
            this.auth.stoploader();
            if (
              data.json() &&
              data.json().status == 1 &&
              data.json().menu_item_data &&
              data.json().menu_item_data.length
            ) {
              this.navCtrl.push("MorePage", {
                searchdata: data.json().menu_item_data[0]
              });
            }
          },
          errorHandler => {
            this.auth.stoploader();
          }
        );
    } catch (err) {
      this.auth.stoploader();
    }
  }
}
