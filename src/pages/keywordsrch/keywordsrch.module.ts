import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { KeywordsrchPage } from "./keywordsrch";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [KeywordsrchPage],
  imports: [IonicPageModule.forChild(KeywordsrchPage), PipesModule]
})
export class KeywordsrchPageModule {}
