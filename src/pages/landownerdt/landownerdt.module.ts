import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandownerdtPage } from './landownerdt';

@NgModule({
  declarations: [
    LandownerdtPage,
  ],
  imports: [
    IonicPageModule.forChild(LandownerdtPage),
  ],
})
export class LandownerdtPageModule {}
