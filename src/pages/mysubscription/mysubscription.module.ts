import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MysubscriptionPage } from './mysubscription';

@NgModule({
  declarations: [
    MysubscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(MysubscriptionPage),
  ],
})
export class MysubscriptionPageModule {}
