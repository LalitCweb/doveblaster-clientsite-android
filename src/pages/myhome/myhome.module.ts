import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { MyhomePage } from "./myhome";
import { AgmCoreModules } from "../../agm/core";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [MyhomePage],
  imports: [AgmCoreModules, PipesModule, IonicPageModule.forChild(MyhomePage)]
})
export class MyhomePageModule {}
