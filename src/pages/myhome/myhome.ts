import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { AngularFirestore, QuerySnapshot } from "angularfire2/firestore";
import * as firebase from "firebase/app";
/**
 * Generated class for the MyhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-myhome",
  templateUrl: "myhome.html"
})
export class MyhomePage {
  options: any = {
    lat: 30.7046,
    lng: 76.7179,
    zoom: 14,
    fillColor: "#DC143C",
    draggable: true,
    editable: true,
    visible: true,
    strokeWeight: 12,
    strokePos: 0
  };
  posts: AngularFireList<any>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: AngularFireDatabase,
    public afs: AngularFirestore
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyhomePage");
  }
  ionViewWillEnter() {
    this.getdata();
  }
  slotdata: any = [];
  mydata: any = [447, 448]
  getdata() {

    // this.slotdata = this.db.list("/260702").valueChanges();
    // this.slotdata = this.db
    //   .list("/dayleasingdata", ref =>
    //     ref
    //       .orderByChild("userid")
    //       .equalTo(447)
    //       .limitToLast(1)
    //   )
    //   .valueChanges();

    for(var x = 0; x<this.mydata.length; x++){
      this.slotdata[x] = this.db
              .list("/dayleasingdata", ref =>
                ref
                  .orderByChild("userid")
                  .equalTo(this.mydata[x])
                  .limitToLast(1)
              )
              .valueChanges();
    }
   /* this.slotdata = this.db
      .list("/dayleasingdata", ref =>
        ref
          .orderByChild("userid")
          .equalTo(447)
          .limitToLast(1)
      )
      .valueChanges();

    this.slotdata = this.db
      .list("/dayleasingdata", ref =>
        ref
          .orderByChild("userid")
          .equalTo(448)
          .limitToLast(1)
      )
      .valueChanges(); */
    console.log(this.slotdata);
  }
  checktime(marker) {
    var timeDiff = Math.abs(
      new Date().getTime() - new Date(marker.params.datetime).getTime()
    );
    var diffDays = Math.ceil(timeDiff / 1000);
    if (diffDays > 5) {
      return false;
    } else {
      return true;
    }
  }
  checktime2(marker) {
    var diffDays = new Date().getTime() - marker;
    if (diffDays > 10000) {
      return false;
    } else {
      return true;
    }
  }
  clearData() {
    this.db.list("/").remove();
  }
}
