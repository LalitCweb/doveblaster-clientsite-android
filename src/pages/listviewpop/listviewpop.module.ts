import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListviewpopPage } from './listviewpop';

@NgModule({
  declarations: [
    ListviewpopPage,
  ],
  imports: [
    IonicPageModule.forChild(ListviewpopPage),
  ],
})
export class ListviewpopPageModule {}
