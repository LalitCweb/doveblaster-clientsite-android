import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ActionSheetController
} from "ionic-angular";

/**
 * Generated class for the ListviewpopPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-listviewpop",
  templateUrl: "listviewpop.html"
})
export class ListviewpopPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController
  ) {}

  ionViewDidLoad() {}

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Sort By",
      buttons: [
        {
          text: "Price High to Low",
          role: "destructive",
          handler: () => {}
        },
        {
          text: "Price Low to High",
          handler: () => {}
        },
        {
          text: "Default",
          handler: () => {}
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }
}
