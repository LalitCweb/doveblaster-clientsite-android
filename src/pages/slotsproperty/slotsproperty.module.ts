import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlotspropertyPage } from './slotsproperty';

@NgModule({
  declarations: [
    SlotspropertyPage,
  ],
  imports: [
    IonicPageModule.forChild(SlotspropertyPage),
  ],
})
export class SlotspropertyPageModule {}
