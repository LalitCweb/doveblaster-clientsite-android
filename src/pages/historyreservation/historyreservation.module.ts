import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { HistoryreservationPage } from "./historyreservation";
import { AgmCoreModules } from "../../agm/core";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [HistoryreservationPage],
  imports: [
    AgmCoreModules,
    PipesModule,
    IonicPageModule.forChild(HistoryreservationPage)
  ]
})
export class HistoryreservationPageModule {}
