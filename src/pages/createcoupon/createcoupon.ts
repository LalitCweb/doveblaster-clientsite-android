import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthProvider } from "../../providers/auth/auth";
import { IMyDpOptions } from "mydatepicker";
/**
 * Generated class for the CreatecouponPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var Keyboard: any;

@IonicPage()
@Component({
  selector: "page-createcoupon",
  templateUrl: "createcoupon.html"
})
export class CreatecouponPage {
  myDatePickerOptions: IMyDpOptions = {
    // other options...
    // editableDateField: true,
    dateFormat: "dd-mm-yyyy"
    // disableDateRanges: [
    //   {
    //     begin: { year: 1900, month: 1, day: 1 },
    //     end: { year: 2999, month: 12, day: 31 }
    //   }
    // ] // disable all
    // enableDays: []
  };
  today: any = new Date().toISOString();
  myForm: FormGroup;
  edit: any = false;
  data: any = {
    coupon_id: "",
    coupon_code: "",
    discount_type: "",
    coupon_amount: "",
    usage_limit: "",
    expire_date: "",
    handling_charges: false
  };
  startdate: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public auth: AuthProvider
  ) {
    var dt: any = new Date();
    this.startdate = {
      date: {
        day: dt.getDate(),
        month: dt.getMonth() + 1,
        year: dt.getFullYear()
      }
    };
    this.myForm = this.fb.group({
      coupon_id: [""],
      coupon_code: ["", Validators.required],
      discount_type: ["", Validators.required],
      coupon_amount: ["", Validators.required],
      usage_limit: ["", Validators.required],
      expire_date: [this.startdate, Validators.required],
      handling_charges: [false],
      restrict_one_coupn_per_day: [false],
      restrict_one_coupn_per_days: [],
      number_of_consecutive_days_for_coupon: ["", Validators.required]
    });
    if (this.navParams.get("single_coupon")) {
      this.edit = true;
      this.myForm.controls["coupon_id"].setValue(
        this.navParams.get("single_coupon").coupon_id
      );
      this.myForm.controls["coupon_code"].setValue(
        this.navParams.get("single_coupon").coupon_name
      );
      this.myForm.controls["discount_type"].setValue(
        this.navParams.get("single_coupon").data.discount_type[0]
      );
      this.myForm.controls["coupon_amount"].setValue(
        this.navParams.get("single_coupon").data.coupon_amount[0]
      );
      this.myForm.controls["usage_limit"].setValue(
        this.navParams.get("single_coupon").data.usage_limit[0]
      );
      this.myForm.controls["number_of_consecutive_days_for_coupon"].setValue(
        parseInt(
          this.navParams.get("single_coupon").data
            .number_of_consecutive_days_for_coupon[0]
        )
      );
      if (
        this.navParams.get("single_coupon").data
          .coupon_to_remove_handlingcharges[0] == 0
      ) {
        this.myForm.controls["handling_charges"].setValue(false);
      } else {
        this.myForm.controls["handling_charges"].setValue(true);
      }

      if (
        this.navParams.get("single_coupon").data
          .restrict_one_coupn_per_day[0] == 0
      ) {
        this.myForm.controls["restrict_one_coupn_per_day"].setValue(false);
      } else {
        this.myForm.controls["restrict_one_coupn_per_day"].setValue(true);
      }
      var dtt: any = new Date(
        this.navParams.get("single_coupon").new_expirydata
      );
      this.startdate = {
        date: {
          day: dtt.getDate(),
          month: dtt.getMonth() + 1,
          year: dtt.getFullYear()
        }
      };
      this.myForm.controls["expire_date"].setValue(this.startdate);
    } else {
      this.data = {};
    }
  }

  datechange(date) {
    setTimeout(() => {
      if (Keyboard.isVisible) {
        Keyboard.hide();
      }
    }, 1000);
  }
  ionViewDidLoad() {}
  userdata: any;
  ionViewWillEnter() {
    this.userdata = JSON.parse(this.auth.getProfile());
  }
  addcoupon() {
    debugger;
    if (this.myForm.value.expire_date == undefined) {
      if (
        this.navParams.get("single_coupon") &&
        this.navParams.get("single_coupon").new_expirydata
      ) {
        this.myForm.controls["expire_date"].setValue(
          this.auth.changeformat2(
            this.navParams.get("single_coupon").new_expirydata.substr(0, 10)
          )
        );
      } else {
        var dtt: any = new Date();
        this.myForm.controls["expire_date"].setValue({
          date: {
            day: dtt.getDate(),
            month: dtt.getMonth() + 1,
            year: dtt.getFullYear()
          }
        });
      }
    } else {
      this.myForm.value.expire_date = this.auth.changeformat4(
        this.myForm.value.expire_date
      );
    }
    debugger;
    setTimeout(() => {
      if (this.myForm.valid) {
        if (this.navParams.get("single_coupon")) {
          this.editcoupon();
        } else {
          this.submitForm();
        }
      }
    }, 800);
  }
  editcoupon() {
    this.auth.startloader();
    if (this.myForm.value.handling_charges == true) {
      this.myForm.value.handling_charges = 1;
      this.myForm.value.coupon_to_remove_handlingcharges = 1;
    } else {
      this.myForm.value.handling_charges = 0;
      this.myForm.value.coupon_to_remove_handlingcharges = 0;
    }
    if (this.myForm.value.restrict_one_coupn_per_day == true) {
      this.myForm.value.restrict_one_coupn_per_days = 1;
    } else {
      this.myForm.value.restrict_one_coupn_per_days = 0;
    }
    try {
      this.auth
        .editcoupon(
          this.auth.getuserId(),
          this.myForm.value.coupon_id,
          this.myForm.value
        )
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              // this.myForm.reset();
              this.auth.toast(data.json().message);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        });
    } catch (err) {
      this.auth.stoploader();
    }
  }
  submitForm() {
    this.auth.startloader();
    if (this.myForm.value.handling_charges == true) {
      this.myForm.value.handling_charges = 1;
      this.myForm.value.coupon_to_remove_handlingcharges = 1;
    } else {
      this.myForm.value.handling_charges = 0;
      this.myForm.value.coupon_to_remove_handlingcharges = 0;
    }
    if (this.myForm.value.restrict_one_coupn_per_day == true) {
      this.myForm.value.restrict_one_coupn_per_days = 1;
    } else {
      this.myForm.value.restrict_one_coupn_per_days = 0;
    }
    try {
      this.auth
        .createcoupon(this.auth.getuserId(), this.myForm.value)
        .subscribe(data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              this.myForm.reset();
              this.auth.toast(data.json().message);
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        });
    } catch (err) {
      this.auth.stoploader();
    }
  }
}
