import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { CreatecouponPage } from "./createcoupon";
import { MyDatePickerModule } from "mydatepicker";

@NgModule({
  declarations: [CreatecouponPage],
  imports: [MyDatePickerModule, IonicPageModule.forChild(CreatecouponPage)]
})
export class CreatecouponPageModule {}
