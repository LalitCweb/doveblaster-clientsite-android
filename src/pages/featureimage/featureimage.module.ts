import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FeatureimagePage } from "./featureimage";
import { ComponentsModule } from "../../components/components.module";
import { PinchZoomModule } from 'ngx-pinch-zoom';

@NgModule({
  declarations: [FeatureimagePage],
  imports: [PinchZoomModule, ComponentsModule, IonicPageModule.forChild(FeatureimagePage)]
})
export class FeatureimagePageModule {}
