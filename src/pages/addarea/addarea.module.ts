import { NgModule, ErrorHandler } from "@angular/core";
import { IonicPageModule, IonicErrorHandler } from "ionic-angular";
import { AddareaPage } from "./addarea";
import { AgmCoreModules } from "../../agm/core";

@NgModule({
  declarations: [AddareaPage],
  imports: [AgmCoreModules, IonicPageModule.forChild(AddareaPage)],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AddareaPageModule {}
