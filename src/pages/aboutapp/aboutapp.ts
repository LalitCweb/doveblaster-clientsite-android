import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
declare var cordova: any;
/**
 * Generated class for the AboutappPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-aboutapp",
  templateUrl: "aboutapp.html"
})
export class AboutappPage {
  appVersion: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    var ell = this;
    cordova.getAppVersion.getVersionNumber(function(version) {
      ell.appVersion.getVersionNumber = version;
    });
    cordova.getAppVersion.getAppName(function(appname) {
      ell.appVersion.getAppName = appname;
    });
    cordova.getAppVersion.getPackageName(function(packagename) {
      ell.appVersion.getPackageName = packagename;
    });
    cordova.getAppVersion.getVersionCode(function(versioncode) {
      ell.appVersion.getVersionCode = versioncode;
    });
  }
  myhomepage() {
    this.navCtrl.push("MyhomePage");
  }
}
