import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from "ionic-angular";
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the MybookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-mybooking",
  templateUrl: "mybooking.html"
})
export class MybookingPage {
  bookings: any = "upcoming";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {}
  ionViewWillEnter() {
    this.userreservation();
  }
  history: any;
  upcoming: any;
  currency: any;
  userreservation() {
    try {
      this.auth.startloader();
      this.auth.userreservation(this.auth.getuserId()).subscribe(
        data => {
          this.auth.stoploader();
          if (data) {
            if (data.json().status == 1) {
              this.history = data.json().past_bookings;
              this.upcoming = data.json().upcoming_bookings;
              this.currency = data.json().currency_symbol;
            } else if (data.json().status == 2) {
              this.auth.logout();
              this.navCtrl.setRoot("LoginPage");
              this.auth.toast("Session expired. Please login again.");
            } else if (data.json() && data.json().status === 0) {
              this.auth.clearcookie();
              this.auth.toast(data.json().message);
            }
          }
        },
        errorHandler => {
          this.auth.logout();
          this.navCtrl.setRoot("LoginPage");
          this.auth.errtoast(errorHandler);
        }
      );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }
  openuser(author) {
    this.navCtrl.push("SingleuserdetailPage", { authorid: author.author });
  }

  confirmcancelbooking(upcomin) {
    console.log(upcomin);
    let alert = this.alertCtrl.create({
      title: "Are you sure",
      message: "You want to cancel booking ?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          handler: () => {}
        },
        {
          text: "Yes",
          handler: () => {
            this.cancelbooking(upcomin);
          }
        }
      ]
    });
    alert.present();
  }

  cancelbooking(upcomin) {
    try {
      this.auth.startloader();
      this.auth
        .cancelorderlandowner(
          upcomin.slot_id,
          upcomin.linked_product_tothis_booking,
          upcomin.linked_order_id_toreservation,
          upcomin.reservationid
        )
        .subscribe(
          data => {
            this.auth.stoploader();
            if (data) {
              if (data.json().status == 1) {
                this.history = data.json().past_bookings;
                this.upcoming = data.json().upcoming_bookings;
                this.currency = data.json().currency_symbol;
              } else if (data.json().status == 2) {
                this.auth.logout();
                this.navCtrl.setRoot("LoginPage");
                this.auth.toast("Session expired. Please login again.");
              } else if (data.json() && data.json().status === 0) {
                this.auth.clearcookie();
                this.auth.toast(data.json().message);
              }
            }
          },
          errorHandler => {
            this.auth.logout();
            this.navCtrl.setRoot("LoginPage");
            this.auth.errtoast(errorHandler);
          }
        );
    } catch (e) {
      this.auth.stoploader();
      this.auth.toast(e);
    }
  }
}
