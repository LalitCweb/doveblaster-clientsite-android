import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { EmailverificationPage } from "./emailverification";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [EmailverificationPage],
  imports: [ComponentsModule, IonicPageModule.forChild(EmailverificationPage)]
})
export class EmailverificationPageModule {}
