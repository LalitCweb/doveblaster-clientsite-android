import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
// import * as $ from "jquery";

declare var $: any;
@IonicPage()
@Component({
  selector: "page-more",
  templateUrl: "more.html"
})
export class MorePage {
  pagedata: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sanitizer: DomSanitizer
  ) {
    this.pagedata = this.navParams.get("searchdata");
    if (this.pagedata.permalink) {
      this.pagedata.permalink = sanitizer.bypassSecurityTrustResourceUrl(
        this.pagedata.permalink
      );
    }
    $("#iframeId")
      .contents()
      .find(".zopim ")
      .css("height", "0");
  }

  ionViewDidLoad() {}
}
