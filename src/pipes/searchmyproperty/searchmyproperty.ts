import { Pipe, PipeTransform } from "@angular/core";

/**
 * Generated class for the SearchmypropertyPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: "search"
})
export class SearchmypropertyPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: any[], terms: string): any[] {
    if (!items) return [];
    if (!terms) return items;
    terms = terms.toLowerCase();
    return items.filter(it => {
      if (it && it.property_name) {
        return it.property_name.toLowerCase().includes(terms); // only filter country name
      } else if (
        it &&
        it.property_details &&
        it.property_details[0] &&
        it.property_details[0].post_title
      ) {
        return it.property_details[0].post_title.toLowerCase().includes(terms); // only filter country name
      }
    });
  }
}
