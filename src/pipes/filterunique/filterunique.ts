import { Pipe, PipeTransform } from "@angular/core";
import * as _ from "lodash";
/**
 * Generated class for the FilteruniquePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: "filterunique"
})
export class FilteruniquePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any): any {
    if (value !== undefined && value !== null) {
      return _.uniqBy(value, "user_id");
    }
    return value;
  }
}
