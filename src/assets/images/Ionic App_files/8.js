webpackJsonp([8],{

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(362);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */])]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bottombar_bottombar__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__bottombar_bottombar__["a" /* BottombarComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__bottombar_bottombar__["a" /* BottombarComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BottombarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the BottombarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BottombarComponent = /** @class */ (function () {
    function BottombarComponent() {
    }
    BottombarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "bottombar",template:/*ion-inline-start:"/Users/webgarhsolutions/Desktop/Project/doveblaster/src/components/bottombar/bottombar.html"*/'<!-- Generated template for the BottombarComponent component -->\n<footer><span>© 2018 GPIDS, LLC all rights reserved</span></footer>\n'/*ion-inline-end:"/Users/webgarhsolutions/Desktop/Project/doveblaster/src/components/bottombar/bottombar.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], BottombarComponent);
    return BottombarComponent;
}());

//# sourceMappingURL=bottombar.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.user = { email: "", password: "" };
        // Disable Side Menu
        this.menuCtrl.enable(false, "myMenu");
    }
    LoginPage.prototype.ngOnInit = function () {
        // Validations Definition
        var EMAILPATTERNS = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        this.configs = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]("", [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(EMAILPATTERNS)
            ]),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]("", [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required])
        });
    };
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.goto = function (page) {
        this.navCtrl.push(page);
    };
    LoginPage.prototype.gotoinfo = function (data) {
        this.navCtrl.push("InfoPage", { info: data });
    };
    LoginPage.prototype.login = function () {
        if (this.configs.valid) {
            this.navCtrl.setRoot("PropertieslistviewPage");
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-login",template:/*ion-inline-start:"/Users/webgarhsolutions/Desktop/Project/doveblaster/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content class="sky-background">\n  <div padding>\n    <div class="logo">\n      <img class="aligncenter" src="assets/images/logo.png" />\n    </div>\n    <div class="loginform">\n      <form (ngSubmit)="login()" [formGroup]="configs" novalidate>\n        <ion-list>\n          <div\n            class="error"\n            *ngIf="\n              configs.get(\'email\').hasError(\'required\') &&\n              configs.get(\'email\').touched\n            "\n          >\n            <p>Email Required</p>\n          </div>\n          <div\n            class="error"\n            *ngIf="\n              configs.get(\'email\').hasError(\'pattern\') &&\n              configs.get(\'email\').touched\n            "\n          >\n            <p>Incorrect Email</p>\n          </div>\n          <div\n            class="error"\n            *ngIf="\n              configs.get(\'password\').hasError(\'required\') &&\n              configs.get(\'password\').touched\n            "\n          >\n            <p>Password Required</p>\n          </div>\n          <ion-item class="inputfield">\n            <i item-start> <ion-icon name="mail"></ion-icon> </i>\n            <ion-input\n              type="email"\n              [(ngModel)]="user.email"\n              name="email"\n              placeholder="Email"\n              formControlName="email"\n              [class.error1]="\n                !configs.controls.email.valid && configs.controls.email.dirty\n              "\n            ></ion-input>\n          </ion-item>\n\n          <ion-item class="inputfield">\n            <i item-start> <ion-icon name="lock"></ion-icon> </i>\n            <ion-input\n              type="password"\n              [(ngModel)]="user.password"\n              name="password"\n              placeholder="Password"\n              formControlName="password"\n              [class.error1]="\n                !configs.controls.password.valid &&\n                configs.controls.password.dirty\n              "\n            ></ion-input>\n          </ion-item>\n        </ion-list>\n        <button\n          ion-button\n          class="submit-button"\n          color="darkora"\n          type="submit"\n          full\n        >\n          Sign In\n        </button>\n      </form>\n      <div class="forgot-password" (click)="goto(\'ForgetpasswordPage\')">\n        Forgot Your Password?\n      </div>\n      <div class="newmember"><span>New Member</span></div>\n      <button\n        type="button"\n        class="buttonblock createaccount"\n        (click)="goto(\'SignupPage\')"\n      >\n        Create Account\n      </button>\n      <button\n        type="button"\n        class="buttonblock landaccount"\n        (click)="goto(\'SignuplandownerPage\')"\n      >\n        Create Land Owner Account\n      </button>\n      <p class="agree">\n        By Signing in you are agreeing to our terms listed in the\n        <em (click)="gotoinfo(\'legal\')">legal information section</em> and\n        <em (click)="gotoinfo(\'policy\')">privacy notice.</em>\n      </p>\n    </div>\n  </div>\n</ion-content>\n<ion-footer\n  ><ion-toolbar><bottombar></bottombar></ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/webgarhsolutions/Desktop/Project/doveblaster/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=8.js.map